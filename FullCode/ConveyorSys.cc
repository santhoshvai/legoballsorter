#include "ConveyorSys.hh"

namespace dezyne
{
  ConveyorSys::ConveyorSys(const dezyne::locator& dezyne_locator)
  : meta{{reinterpret_cast<component*>(&conveyor),reinterpret_cast<component*>(&beltMotor),reinterpret_cast<component*>(&exitSensor)}, 0, reinterpret_cast<component*>(this), ""}
  , conveyor(dezyne_locator)
  , beltMotor(dezyne_locator)
  , exitSensor(dezyne_locator)
  , port(conveyor.conveyor)
  {
    conveyor.meta.parent = reinterpret_cast<component*>(this);
    conveyor.meta.address = reinterpret_cast<component*>(&conveyor);
    conveyor.meta.name = "conveyor";
    beltMotor.meta.parent = reinterpret_cast<component*>(this);
    beltMotor.meta.address = reinterpret_cast<component*>(&beltMotor);
    beltMotor.meta.name = "beltMotor";
    exitSensor.meta.parent = reinterpret_cast<component*>(this);
    exitSensor.meta.address = reinterpret_cast<component*>(&exitSensor);
    exitSensor.meta.name = "exitSensor";
    connect(beltMotor.port, conveyor.beltMotor);
    connect(exitSensor.port, conveyor.exitSensor);
  }
}
