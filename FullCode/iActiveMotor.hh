#ifndef DEZYNE_IACTIVEMOTOR_HH
#define DEZYNE_IACTIVEMOTOR_HH

#include <boost/bind.hpp>
#include <boost/function.hpp>
#include "lego_usb.hh"

namespace dezyne
{
  struct iActiveMotor
  {

    struct
    {
      boost::function<void (lego::USB::Device* comm, int port)> RotationCount;
      boost::function<void (lego::USB::Device* comm, int port, bool relative)> ResetCount;
      boost::function<void (lego::USB::Device* comm, int port, int power)> Forward;
      boost::function<void (lego::USB::Device* comm, int port, int power)> Reverse;
      boost::function<void (lego::USB::Device* comm, int port, bool brake)> Stop;
      boost::function<void (lego::USB::Device* comm, int port, int power, int tacho, bool brake)> GoTo;

      struct
      {
        const char* component;
        const char* port;
        void*       address;
      } meta;
    } in;

    struct
    {
      boost::function<void ()> taskPerformed;

      struct
      {
        const char* component;
        const char* port;
        void*       address;
      } meta;
    } out;
  };

  inline void connect (iActiveMotor& provided, iActiveMotor& required)
  {
    assert (not required.in.RotationCount);
    assert (not required.in.ResetCount);
    assert (not required.in.Forward);
    assert (not required.in.Reverse);
    assert (not required.in.Stop);
    assert (not required.in.GoTo);

    assert (not provided.out.taskPerformed);

    provided.out = required.out;
    required.in = provided.in;
  }
}
#endif // DEZYNE_IACTIVEMOTOR_HH
