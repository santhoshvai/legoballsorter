#ifndef DEZYNE_FEEDER_HH
#define DEZYNE_FEEDER_HH

#include "iFeeder.hh"
#include "iActiveMotor.hh"
#include "iActiveSensor.hh"


#include "runtime.hh"

namespace dezyne
{
  struct locator;
  struct runtime;

  struct Feeder
  {
    dezyne::meta meta;
    runtime& rt;
    struct connState
    {
      enum type
      {
        Initialising, Calibrating, Active
      };
      static const char* to_string(type v)
      {
        switch(v)
        {
          case Initialising: return "connState_Initialising";
          case Calibrating: return "connState_Calibrating";
          case Active: return "connState_Active";

        }
        return "";
      }
    };
    typedef int cnt;
    Feeder::cnt wafers;
    lego::USB::Device* commFeed;
    lego::USB::Device* commPickUp;
    int portballsSensor;
    int porttrackLeftSensor;
    int portbeltSensor;
    int portPickUpSensor;
    int portballM;
    int portBeltM;
    int pow;
    int degBallM;
    int degBeltM;
    bool rel;
    bool brake;
    Feeder::connState::type c;
    iFeeder feeder;
    iActiveMotor ballsMotor;
    iActiveMotor beltMotor;
    iActiveSensor ballsSensor;
    iActiveSensor beltSensor;

    Feeder(const locator&);

    private:
    void feeder_init(lego::USB::Device* commfeed, lego::USB::Device* commpickup);
    void feeder_push();
    void ballsMotor_taskPerformed();
    void beltMotor_taskPerformed();
    void ballsSensor_sOpened();
    void ballsSensor_sClosed();
    void beltSensor_sOpened();
    void beltSensor_sClosed();
  };
}
#endif // DEZYNE_FEEDER_HH
