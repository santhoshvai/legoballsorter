#ifndef DEZYNE_IFEEDER_HH
#define DEZYNE_IFEEDER_HH

#include <boost/bind.hpp>
#include <boost/function.hpp>
#include "lego_usb.hh"

namespace dezyne
{
  struct iFeeder
  {

    struct
    {
      boost::function<void (lego::USB::Device* commfeed, lego::USB::Device* commpickup)> init;
      boost::function<void ()> push;

      struct
      {
        const char* component;
        const char* port;
        void*       address;
      } meta;
    } in;

    struct
    {
      boost::function<void ()> ballFeeded;

      struct
      {
        const char* component;
        const char* port;
        void*       address;
      } meta;
    } out;
  };

  inline void connect (iFeeder& provided, iFeeder& required)
  {
    assert (not required.in.init);
    assert (not required.in.push);

    assert (not provided.out.ballFeeded);

    provided.out = required.out;
    required.in = provided.in;
  }
}
#endif // DEZYNE_IFEEDER_HH
