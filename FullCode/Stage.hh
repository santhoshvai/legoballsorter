#ifndef DEZYNE_STAGE_HH
#define DEZYNE_STAGE_HH

#include "iStage.hh"
#include "iActiveMotor.hh"
#include "iActiveSensor.hh"
#include "iColourSensor.hh"


#include "runtime.hh"

namespace dezyne
{
  struct locator;
  struct runtime;

  struct Stage
  {
    dezyne::meta meta;
    runtime& rt;
    struct connState
    {
      enum type
      {
        Initialising, Busy, Active, Inspected, unLoaded
      };
      static const char* to_string(type v)
      {
        switch(v)
        {
          case Initialising: return "connState_Initialising";
          case Busy: return "connState_Busy";
          case Active: return "connState_Active";
          case Inspected: return "connState_Inspected";
          case unLoaded: return "connState_unLoaded";

        }
        return "";
      }
    };
    typedef int andStageInt;
    Stage::andStageInt calibrated;
    bool ballBool;
    bool ballYHomeBool;
    lego::USB::Device* commStage;
    lego::USB::Device* commFeed;
    lego::USB::Device* commConveyor;
    int portstageYHomeSensor;
    int portstageYEndSensor;
    int portlightSensor;
    int portstageXHomeSensor;
    int portstageXEndSensor;
    int portstageXMotor;
    int portstageYMotor;
    int pow;
    int degstageReceiveYMotor;
    int degstageReceiveXMotor;
    int degstageInspectYMotor;
    int degstageInspectXMotor;
    int degstageAcceptYMotor;
    int degstageAcceptXMotor;
    int degstageRejectYMotor;
    int degstageRejectXMotor;
    bool rel;
    bool brake;
    Stage::connState::type c;
    iStage stage;
    iActiveMotor stageXMotor;
    iActiveMotor stageYMotor;
    iActiveSensor stageYHomeSensor;
    iActiveSensor stageYEndSensor;
    iActiveSensor stageXHomeSensor;
    iActiveSensor stageXEndSensor;
    iColourSensor lightSensor;

    Stage(const locator&);

    private:
    void stage_init(lego::USB::Device* commstage, lego::USB::Device* commfeed, lego::USB::Device* commconveyor);
    void stage_prepareStage();
    void stage_unLoaded();
    void stageXMotor_taskPerformed();
    void stageYMotor_taskPerformed();
    void stageYHomeSensor_sOpened();
    void stageYHomeSensor_sClosed();
    void stageYEndSensor_sOpened();
    void stageYEndSensor_sClosed();
    void stageXHomeSensor_sOpened();
    void stageXHomeSensor_sClosed();
    void stageXEndSensor_sOpened();
    void stageXEndSensor_sClosed();
    void lightSensor_redFail();
    void lightSensor_bluePass();
    void calibrate();
  };
}
#endif // DEZYNE_STAGE_HH
