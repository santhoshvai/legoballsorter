#ifndef DEZYNE_MOTOR_HH
#define DEZYNE_MOTOR_HH

#include "iMotor.hh"


#include "runtime.hh"

namespace dezyne
{
  struct locator;
  struct runtime;

  struct Motor
  {
    dezyne::meta meta;
    runtime& rt;
    iMotor::MovingState::type reply_iMotor_MovingState;
    iMotor port;

    Motor(const locator&);

    private:
    void port_GetRotationCount(lego::USB::Device* commM, int port);
    void port_ResetRotationCount(lego::USB::Device* commM, int port, bool relative);
    void port_SetForward(lego::USB::Device* commM, int port, int power);
    void port_SetReverse(lego::USB::Device* commM, int port, int power);
    void port_Stop(lego::USB::Device* commM, int port, bool brake);
    void port_GoTo(lego::USB::Device* commM, int port, int power, int tacho, bool brake);
  };
}
#endif // DEZYNE_MOTOR_HH
