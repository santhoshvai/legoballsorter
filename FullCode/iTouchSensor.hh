#ifndef DEZYNE_ITOUCHSENSOR_HH
#define DEZYNE_ITOUCHSENSOR_HH

#include <boost/bind.hpp>
#include <boost/function.hpp>
#include "lego_usb.hh"

namespace dezyne
{
  struct iTouchSensor
  {
    struct SensorValue
    {
      enum type
      {
        Opened, Closed
      };
      static const char* to_string(type v)
      {
        switch(v)
        {
          case Opened: return "SensorValue_Opened";
          case Closed: return "SensorValue_Closed";

        }
        return "";
      }
    };

    struct
    {
      boost::function<SensorValue::type (lego::USB::Device* commTS, int portTS)> GetValue;
      boost::function<void (lego::USB::Device* commTS, int portTS)> SetTouch;

      struct
      {
        const char* component;
        const char* port;
        void*       address;
      } meta;
    } in;

    struct
    {

      struct
      {
        const char* component;
        const char* port;
        void*       address;
      } meta;
    } out;
  };

  inline void connect (iTouchSensor& provided, iTouchSensor& required)
  {
    assert (not required.in.GetValue);
    assert (not required.in.SetTouch);


    provided.out = required.out;
    required.in = provided.in;
  }
}
#endif // DEZYNE_ITOUCHSENSOR_HH
