#ifndef DEZYNE_ICOLOURSENSOR_HH
#define DEZYNE_ICOLOURSENSOR_HH

#include <boost/bind.hpp>
#include <boost/function.hpp>
#include "lego_usb.hh"

namespace dezyne
{
  struct iColourSensor
  {

    struct
    {
      boost::function<void (lego::USB::Device* commCS, int portCS)> GetValue;
      boost::function<void (lego::USB::Device* commCS, int portCS)> SetTouch;

      struct
      {
        const char* component;
        const char* port;
        void*       address;
      } meta;
    } in;

    struct
    {
      boost::function<void ()> redFail;
      boost::function<void ()> bluePass;

      struct
      {
        const char* component;
        const char* port;
        void*       address;
      } meta;
    } out;
  };

  inline void connect (iColourSensor& provided, iColourSensor& required)
  {
    assert (not required.in.GetValue);
    assert (not required.in.SetTouch);

    assert (not provided.out.redFail);
    assert (not provided.out.bluePass);

    provided.out = required.out;
    required.in = provided.in;
  }
}
#endif // DEZYNE_ICOLOURSENSOR_HH
