#ifndef DEZYNE_CONTROLLER_HH
#define DEZYNE_CONTROLLER_HH

#include "IMain.hh"
#include "iMaterialHandler.hh"
#include "iStage.hh"
#include "iConveyor.hh"
#include "iFeeder.hh"


#include "runtime.hh"

namespace dezyne
{
  struct locator;
  struct runtime;

  struct Controller
  {
    dezyne::meta meta;
    runtime& rt;
    struct connState
    {
      enum type
      {
        Initialising, Busy, AfterUnload
      };
      static const char* to_string(type v)
      {
        switch(v)
        {
          case Initialising: return "connState_Initialising";
          case Busy: return "connState_Busy";
          case AfterUnload: return "connState_AfterUnload";

        }
        return "";
      }
    };
    bool ballBool;
    lego::USB::Device* commFeed;
    lego::USB::Device* commPickUp;
    lego::USB::Device* commConveyor;
    lego::USB::Device* commStage;
    struct initConveyor* acceptStruct;
    struct initConveyor* rejectStruct;
    Controller::connState::type c;
    IMain port;
    iMaterialHandler handler;
    iStage stage;
    iConveyor accept;
    iConveyor reject;
    iFeeder feeder;

    Controller(const locator&);

    private:
    void port_init(lego::USB::Device* commpickup, lego::USB::Device* commfeed, lego::USB::Device* commstage, lego::USB::Device* commconveyor, struct initConveyor* acceptstruct, struct initConveyor* rejectstruct);
    void handler_prepareStage();
    void handler_unLoaded();
    void handler_startConveyor();
    void stage_ballPass();
    void stage_ballFail();
    void feeder_ballFeeded();
    void initAll();
  };
}
#endif // DEZYNE_CONTROLLER_HH
