#include "Handler.hh"

#include "locator.hh"
#include "runtime.hh"

#include <iostream>
#include <cstdlib>

namespace dezyne
{
  Handler::Handler(const locator& dezyne_locator)
  : rt(dezyne_locator.get<runtime>())
  , ballHandlingCount(0)
  , prepareToUnloadStageInt(0)
  , goBackToInputPosInt(0)
  , workToDo(1)
  , hoistCalibrated(false)
  , trackCalibrated(false)
  , trolleyCalibrated(false)
  , ballBool(false)
  , commPickUp()
  , commFeed()
  , commConveyor()
  , portHoist(OUT_A)
  , portGripper(OUT_B)
  , portEndTrack(OUT_C)
  , portTrolleyM(OUT_A)
  , portPickUpSensor(IN_3)
  , portExtenderHomeSwitchSensor(IN_2)
  , porttrackLeftSensor(IN_3)
  , portGripperSensor(IN_1)
  , pow(10)
  , degEndTrackToGrabPosFromInput(Position::Robot::X::InputPick)
  , degEndTrackToStageBeforeInsp(Position::Robot::X::InputDrop)
  , degEndTrackUnloadStage(Position::Robot::X::OutputPick)
  , degEndTrackDeliverToConveyer(Position::Robot::X::OutputDrop)
  , degTrolleyGrabPosFromInput(Position::Robot::Y::InputPick)
  , degTrolleyDeliverPosStageBeforeInsp(Position::Robot::Y::InputDrop)
  , degTrolleyUnloadStagePass(Position::Robot::Y::AcceptPick)
  , degTrolleyUnloadStageFail(Position::Robot::Y::RejectPick)
  , degTrolleyDeliverToConveyerPass(Position::Robot::Y::AcceptDrop)
  , degTrolleyDeliverToConveyerFail(Position::Robot::Y::RejectDrop)
  , degGripper(210)
  , degHoist(Position::Robot::Z::Down)
  , rel(false)
  , brake(true)
  , c(connState::Initialising)
  , handler()
  , EndTrackMotor()
  , GripperMotor()
  , HoistMotor()
  , trolleyMotor()
  , trackLeftSensor()
  , pickupSensor()
  , ExtenderHomeSwitchSensor()
  , GripperSensor()
  {
    handler.in.meta.component = "Handler";
    handler.in.meta.port = "handler";
    handler.in.meta.address = this;
    EndTrackMotor.out.meta.component = "Handler";
    EndTrackMotor.out.meta.port = "EndTrackMotor";
    EndTrackMotor.out.meta.address = this;
    GripperMotor.out.meta.component = "Handler";
    GripperMotor.out.meta.port = "GripperMotor";
    GripperMotor.out.meta.address = this;
    HoistMotor.out.meta.component = "Handler";
    HoistMotor.out.meta.port = "HoistMotor";
    HoistMotor.out.meta.address = this;
    trolleyMotor.out.meta.component = "Handler";
    trolleyMotor.out.meta.port = "trolleyMotor";
    trolleyMotor.out.meta.address = this;
    trackLeftSensor.out.meta.component = "Handler";
    trackLeftSensor.out.meta.port = "trackLeftSensor";
    trackLeftSensor.out.meta.address = this;
    pickupSensor.out.meta.component = "Handler";
    pickupSensor.out.meta.port = "pickupSensor";
    pickupSensor.out.meta.address = this;
    ExtenderHomeSwitchSensor.out.meta.component = "Handler";
    ExtenderHomeSwitchSensor.out.meta.port = "ExtenderHomeSwitchSensor";
    ExtenderHomeSwitchSensor.out.meta.address = this;
    GripperSensor.out.meta.component = "Handler";
    GripperSensor.out.meta.port = "GripperSensor";
    GripperSensor.out.meta.address = this;

    handler.in.init = connect<lego::USB::Device*,lego::USB::Device*,lego::USB::Device*>(rt, this,
    boost::function<void(lego::USB::Device*,lego::USB::Device*,lego::USB::Device*)>
    ([this] (lego::USB::Device* commpickup, lego::USB::Device* commfeed, lego::USB::Device* commconveyor)
    {
      trace (handler, "init");
      handler_init(commpickup,commfeed,commconveyor);
      trace_return (handler, "return");
      return;
    }
    ));
    handler.in.ballFeeded = connect<void>(rt, this,
    boost::function<void()>
    ([this] ()
    {
      trace (handler, "ballFeeded");
      handler_ballFeeded();
      trace_return (handler, "return");
      return;
    }
    ));
    handler.in.ballFail = connect<void>(rt, this,
    boost::function<void()>
    ([this] ()
    {
      trace (handler, "ballFail");
      handler_ballFail();
      trace_return (handler, "return");
      return;
    }
    ));
    handler.in.ballPass = connect<void>(rt, this,
    boost::function<void()>
    ([this] ()
    {
      trace (handler, "ballPass");
      handler_ballPass();
      trace_return (handler, "return");
      return;
    }
    ));
    EndTrackMotor.out.taskPerformed=  [this] () {
      trace (EndTrackMotor, "taskPerformed");
      rt.defer (EndTrackMotor.in.meta.address, connect<void>(rt, this,
      boost::function<void()>(
      [=]
      {
        EndTrackMotor_taskPerformed();
        return;
      }
      )));};
    GripperMotor.out.taskPerformed=  [this] () {
      trace (GripperMotor, "taskPerformed");
      rt.defer (GripperMotor.in.meta.address, connect<void>(rt, this,
      boost::function<void()>(
      [=]
      {
        GripperMotor_taskPerformed();
        return;
      }
      )));};
    HoistMotor.out.taskPerformed=  [this] () {
      trace (HoistMotor, "taskPerformed");
      rt.defer (HoistMotor.in.meta.address, connect<void>(rt, this,
      boost::function<void()>(
      [=]
      {
        HoistMotor_taskPerformed();
        return;
      }
      )));};
    trolleyMotor.out.taskPerformed=  [this] () {
      trace (trolleyMotor, "taskPerformed");
      rt.defer (trolleyMotor.in.meta.address, connect<void>(rt, this,
      boost::function<void()>(
      [=]
      {
        trolleyMotor_taskPerformed();
        return;
      }
      )));};
    trackLeftSensor.out.sOpened=  [this] () {
      trace (trackLeftSensor, "sOpened");
      rt.defer (trackLeftSensor.in.meta.address, connect<void>(rt, this,
      boost::function<void()>(
      [=]
      {
        trackLeftSensor_sOpened();
        return;
      }
      )));};
    trackLeftSensor.out.sClosed=  [this] () {
      trace (trackLeftSensor, "sClosed");
      rt.defer (trackLeftSensor.in.meta.address, connect<void>(rt, this,
      boost::function<void()>(
      [=]
      {
        trackLeftSensor_sClosed();
        return;
      }
      )));};
    pickupSensor.out.sOpened=  [this] () {
      trace (pickupSensor, "sOpened");
      rt.defer (pickupSensor.in.meta.address, connect<void>(rt, this,
      boost::function<void()>(
      [=]
      {
        pickupSensor_sOpened();
        return;
      }
      )));};
    pickupSensor.out.sClosed=  [this] () {
      trace (pickupSensor, "sClosed");
      rt.defer (pickupSensor.in.meta.address, connect<void>(rt, this,
      boost::function<void()>(
      [=]
      {
        pickupSensor_sClosed();
        return;
      }
      )));};
    ExtenderHomeSwitchSensor.out.sOpened=  [this] () {
      trace (ExtenderHomeSwitchSensor, "sOpened");
      rt.defer (ExtenderHomeSwitchSensor.in.meta.address, connect<void>(rt, this,
      boost::function<void()>(
      [=]
      {
        ExtenderHomeSwitchSensor_sOpened();
        return;
      }
      )));};
    ExtenderHomeSwitchSensor.out.sClosed=  [this] () {
      trace (ExtenderHomeSwitchSensor, "sClosed");
      rt.defer (ExtenderHomeSwitchSensor.in.meta.address, connect<void>(rt, this,
      boost::function<void()>(
      [=]
      {
        ExtenderHomeSwitchSensor_sClosed();
        return;
      }
      )));};
    GripperSensor.out.sOpened=  [this] () {
      trace (GripperSensor, "sOpened");
      rt.defer (GripperSensor.in.meta.address, connect<void>(rt, this,
      boost::function<void()>(
      [=]
      {
        GripperSensor_sOpened();
        return;
      }
      )));};
    GripperSensor.out.sClosed=  [this] () {
      trace (GripperSensor, "sClosed");
      rt.defer (GripperSensor.in.meta.address, connect<void>(rt, this,
      boost::function<void()>(
      [=]
      {
        GripperSensor_sClosed();
        return;
      }
      )));};
  }

  void Handler::handler_init(lego::USB::Device* commpickup, lego::USB::Device* commfeed, lego::USB::Device* commconveyor)
  {
    if (c == connState::Initialising)
    {
      commPickUp = commpickup;
      commFeed = commfeed;
      commConveyor = commconveyor;
      ExtenderHomeSwitchSensor.in.setTouch(commPickUp, portExtenderHomeSwitchSensor);
      pickupSensor.in.setTouch(commPickUp, portPickUpSensor);
      trackLeftSensor.in.setTouch(commFeed, porttrackLeftSensor);
      GripperSensor.in.setTouch(commPickUp, portGripperSensor);
      calibrate();
    }
    else if (not (c == connState::Initialising))
    {
    }
  }

  void Handler::handler_ballFeeded()
  {
    {
        std::cout << ("\033[31m_ballFeeded_\033[0m") << std::endl;
        workToDo = 1;
      pickUpBallFromInput ();
    }
  }

  void Handler::handler_ballFail()
  {
      std::cout << ("\033[33m_HAndler-BallFail_\033[0m") << std::endl;
    if (c == connState::Busy)
    {
      if (workToDo < 4)
      {
        ballBool = false;
        workToDo = 5;
      }
      unloadBallFromStageFail ();
    }
    else if (not (c == connState::Busy))
    {
    }
  }

  void Handler::handler_ballPass()
  {
    if (c == connState::Busy)
    {
      if (workToDo < 4)
      {
        ballBool = true;
        workToDo = 4;
      }
      unloadBallFromStagePass ();
    }
    else if (not (c == connState::Busy))
    {
    }
  }

  void Handler::EndTrackMotor_taskPerformed()
  {
      std::cout << ("\033[35m_EndTrackMotor_taskPerformed_\033[0m") << std::endl;
    if (c == connState::PickUp)
    {
      {
        pickUpBall ();
      }
    }
    else if (c == connState::Drop)
    {
      {
        dropBall ();
      }
    }
    else if (not ((c == connState::Drop or c == connState::PickUp)))
    {
      {
      }
    }
  }

  void Handler::GripperMotor_taskPerformed()
  {
      std::cout << ("\033[35m_GripperMotor_taskPerformed_\033[0m") << std::endl;
    if (c == connState::Busy)
    {
      {
        // prepareToUnloadStage ();
        // goBackToInputPos ();
      }
    }
    else if (c == connState::PickUp)
    {
      {
        pickUpBallStep2New ();
      }
    }
    else if (c == connState::Drop)
    {
      {
        dropBallStep2New ();
      }
    }
    else if (c == connState::AfterPickUp)
    {
      {
        waitSync ();
      }
    }
    else if (not ((c == connState::AfterPickUp or (c == connState::Drop or (c == connState::PickUp or c == connState::Busy)))))
    {
      {
      }
    }
  }

  void Handler::HoistMotor_taskPerformed()
  {
      std::cout << ("\033[35m_HoistMotor_taskPerformed_\033[0m") << std::endl;
    if (c == connState::Busy)
    {
      {
        // prepareToUnloadStage ();
        // goBackToInputPos ();
      }
    }
    else if (c == connState::PickUp)
    {
      {
        pickUpBallStep2New ();
      }
    }
    else if (c == connState::Drop)
    {
      {
        dropBallStep2New ();
      }
    }
    else if (c == connState::AfterPickUp)
    {
      {
        waitSync ();
      }
    }
    else if (not ((c == connState::AfterPickUp or (c == connState::Drop or (c == connState::PickUp or c == connState::Busy)))))
    {
      {
      }
    }
  }

  void Handler::trolleyMotor_taskPerformed()
  {
      std::cout << ("\033[35m_trolleyMotor_taskPerformed_\033[0m") << std::endl;
    if (c == connState::PickUp)
    {
      {
        pickUpBall ();
      }
    }
    else if (c == connState::Drop)
    {
      {
        dropBall ();
      }
    }
    else if (not ((c == connState::Drop or c == connState::PickUp)))
    {
      {
      }
    }
  }

  void Handler::trackLeftSensor_sOpened()
  {
    if (c == connState::Initialising)
    {
      assert(false);
    }
    else if (not (c == connState::Initialising))
    {
      {
      }
    }
  }

  void Handler::trackLeftSensor_sClosed()
  {
    if (c == connState::Calibrating)
    {
      trackLeftSensor.in.stopMonitor();
      EndTrackMotor.in.Stop(commPickUp, portEndTrack, brake);
      EndTrackMotor.in.ResetCount(commPickUp, portEndTrack, rel);
      workToDo = 1;
      trackCalibrated = true;
      readycheck ();
    }
    else if (c == connState::Initialising)
    assert(false);
    else if (not ((c == connState::Initialising or c == connState::Calibrating)))
    {
    }
  }

  void Handler::pickupSensor_sOpened()
  {
    if (c == connState::Initialising)
    {
      assert(false);
    }
    else if (not (c == connState::Initialising))
    {
      {
      }
    }
  }

  void Handler::pickupSensor_sClosed()
  {
      std::cout << ("\033[31m_pickupSensor_sClosed_\033[0m") << std::endl;
    if (c == connState::Calibrating)
    {

        HoistMotor.in.Stop(commPickUp, portHoist, brake);
        HoistMotor.in.ResetCount(commPickUp, portHoist, rel);
        pickupSensor.in.stopMonitor();
        hoistCalibrated = true;
        readycheck ();
    }
    else if (c == connState::PickUp)
    {
      if (workToDo > 1) pickupSensor.in.stopMonitor();
    //   dropBallToStageBeforeInsp ();
    //   dropBallToConveyerPass ();
    //   dropBallToConveyerFail ();
    }
    else if (c == connState::AfterPickUp)
    {
      HoistMotor.in.Stop(commPickUp, portHoist, brake);
      HoistMotor.in.ResetCount(commPickUp, portHoist, rel);
      pickupSensor.in.stopMonitor();
      waitSync ();
    }
    else if (c == connState::Busy)
    {
        HoistMotor.in.Stop(commPickUp, portHoist, brake);
        HoistMotor.in.ResetCount(commPickUp, portHoist, rel);
        pickupSensor.in.stopMonitor();
      prepareToUnloadStage ();
      goBackToInputPos ();
    }
    else if (c == connState::Initialising)
    assert(false);
    else if (not ((c == connState::Initialising or (c == connState::Busy or (c == connState::AfterPickUp or (c == connState::PickUp or c == connState::Calibrating))))))
    {
    }
  }

  void Handler::ExtenderHomeSwitchSensor_sOpened()
  {
    if (c == connState::Initialising)
    {
      assert(false);
    }
    else if (not (c == connState::Initialising))
    {
      {
      }
    }
  }

  void Handler::ExtenderHomeSwitchSensor_sClosed()
  {
    if (c == connState::Calibrating)
    {
      trolleyMotor.in.Stop(commFeed, portTrolleyM, brake);
      trolleyMotor.in.ResetCount(commFeed, portTrolleyM, rel);
      ExtenderHomeSwitchSensor.in.stopMonitor();
      trolleyCalibrated = true;
      readycheck ();
    }
    else if (c == connState::Initialising)
    assert(false);
    else if (not ((c == connState::Initialising or c == connState::Calibrating)))
    {
    }
  }

  void Handler::GripperSensor_sOpened()
  {
    {
    }
  }

  void Handler::GripperSensor_sClosed()
  {
      std::cout << ("\033[31m_GripperSensor_sClosed_\033[0m") << std::endl;
    if (c == connState::Calibrating)
    {
        GripperMotor.in.Stop(commPickUp, portGripper, brake);
        GripperMotor.in.ResetCount(commPickUp, portGripper, rel);
        GripperSensor.in.stopMonitor();
    }
    else if (c == connState::AfterPickUp)
    {
      GripperMotor.in.Stop(commPickUp, portGripper, brake);
      GripperMotor.in.ResetCount(commPickUp, portGripper, rel);
      GripperSensor.in.stopMonitor();
      waitSync ();
    }
    else if (c == connState::Busy)
    {
        GripperMotor.in.Stop(commPickUp, portGripper, brake);
        GripperMotor.in.ResetCount(commPickUp, portGripper, rel);
        GripperSensor.in.stopMonitor();
      prepareToUnloadStage ();
      goBackToInputPos ();
    }
  }

  void Handler::calibrate()
  {
    pickupSensor.in.activate();
    HoistMotor.in.Forward(commPickUp, portHoist, pow);
    GripperSensor.in.activate();
    GripperMotor.in.Forward(commPickUp, portGripper, pow);
    ExtenderHomeSwitchSensor.in.activate();
    trolleyMotor.in.Forward(commFeed, portTrolleyM, pow);
    trackLeftSensor.in.activate();
    EndTrackMotor.in.Reverse(commPickUp, portEndTrack, 30);
    c = connState::Calibrating;
    return;
  }

  void Handler::readycheck()
  {
    if (hoistCalibrated and trackCalibrated and trolleyCalibrated)
    {
      workToDo = 1;
      std::cout << ("\033[31m_All 3 calibrated_\033[0m") << std::endl;
      c = connState::Busy;
      pickUpBallFromInput ();
    }
    return;
  }

  void Handler::waitSync()
  {
      std::cout << ("\033[31m_waitSync_\033[0m") << std::endl;
    prepareToUnloadStageInt = prepareToUnloadStageInt + 1;
    if (prepareToUnloadStageInt > 1)
    {
      prepareToUnloadStageInt = 0;
      dropBallToStageBeforeInsp ();
      dropBallToConveyerPass ();
      dropBallToConveyerFail ();
    }
    return;
  }

  void Handler::pickUpBall()
  {
     std::cout << ("\033[32m_pickUpBall_WithoutIF\033[0m") << std::endl;
    ballHandlingCount = ballHandlingCount + 1;
    if ((ballHandlingCount > 1 or workToDo == 1))
    {
      std::cout << ("\033[31m_pickUpBall_\033[0m") << std::endl;
      GripperMotor.in.GoTo(commPickUp, portGripper, -(pow+40), degGripper, brake);
      HoistMotor.in.GoTo(commPickUp, portHoist, pow, degHoist, brake);
      ballHandlingCount = 0;
    }
    return;
  }

  void Handler::pickUpBallStep2()
  {
    ballHandlingCount = ballHandlingCount + 1;
    if (ballHandlingCount > 1)
    {
      GripperMotor.in.GoTo(commPickUp, portGripper, pow, degGripper, brake);
      HoistMotor.in.GoTo(commPickUp, portHoist, pow+40, degHoist, brake);
      ballHandlingCount = 0;
      c = connState::AfterPickUp;
    }
    return;
  }

  void Handler::pickUpBallStep2New()
  {
    ballHandlingCount = ballHandlingCount + 1;
    if (ballHandlingCount > 1)
    {
      std::cout << ("\033[31m_pickUpBallStep2New_\033[0m") << std::endl;
      GripperSensor.in.activate();
      pickupSensor.in.activate();
      GripperMotor.in.Forward(commPickUp, portGripper, pow+40);
      HoistMotor.in.Forward(commPickUp, portHoist, pow);
      ballHandlingCount = 0;
      c = connState::AfterPickUp;
    }
    return;
  }

  void Handler::dropBall()
  {
    ballHandlingCount = ballHandlingCount + 1;
    if (ballHandlingCount > 1)
    {
        std::cout << ("\033[31m_dropBall_\033[0m") << std::endl;
      GripperMotor.in.GoTo(commPickUp, portGripper, -(pow+5), degGripper, brake);
      HoistMotor.in.GoTo(commPickUp, portHoist, (pow-2), degHoist, brake);
      ballHandlingCount = 0;
    }
    return;
  }

  void Handler::dropBallStep2()
  {
    ballHandlingCount = ballHandlingCount + 1;
    if (ballHandlingCount > 1)
    {

      HoistMotor.in.GoTo(commPickUp, portHoist, pow, degHoist, brake);
      GripperMotor.in.GoTo(commPickUp, portGripper, pow+40, degGripper, brake);
      ballHandlingCount = 0;
      c = connState::Busy;
    }
    return;
  }

  void Handler::dropBallStep2New()
  {
    ballHandlingCount = ballHandlingCount + 1;
    if (ballHandlingCount > 1)
    {
        std::cout << ("\033[31m_dropBallStep2New_\033[0m") << std::endl;
      GripperSensor.in.activate();
      pickupSensor.in.activate();
      HoistMotor.in.Forward(commPickUp, portHoist, pow+40);
      GripperMotor.in.Forward(commPickUp, portGripper, (pow-2));
      ballHandlingCount = 0;
      c = connState::Busy;
    }
    return;
  }

  void Handler::pickUpBallFromInput()
  {
    ballHandlingCount = ballHandlingCount + 1;
    if (ballHandlingCount > 1)
    {
      ballHandlingCount = 0;
      if (workToDo == 1)
      {
        std::cout << ("\033[31m_pickUpBallFromInput_\033[0m") << std::endl;
        EndTrackMotor.in.GoTo(commPickUp, portEndTrack, 30, degEndTrackToGrabPosFromInput, brake);
        trolleyMotor.in.GoTo(commFeed, portTrolleyM, (pow+20), degTrolleyGrabPosFromInput, brake);
        workToDo = 2;
        c = connState::PickUp;
      }
    }
    return;
  }

  void Handler::dropBallToStageBeforeInsp()
  {

    if (workToDo == 2)
    {
        std::cout << ("\033[31m_dropBallToStageBeforeInsp_\033[0m") << std::endl;
      trolleyMotor.in.GoTo(commFeed, portTrolleyM, (pow+20), degTrolleyDeliverPosStageBeforeInsp, brake);
      EndTrackMotor.in.GoTo(commPickUp, portEndTrack, 30, degEndTrackToStageBeforeInsp, brake);
      c = connState::Drop;
    }
    return;
  }

  void Handler::prepareToUnloadStage()
  {
    prepareToUnloadStageInt = prepareToUnloadStageInt + 1;
    if (prepareToUnloadStageInt > 1)
    {
      prepareToUnloadStageInt = 0;
      if (workToDo == 2)
      {
        std::cout << ("\033[31m_prepareToUnloadStage_\033[0m") << std::endl;
        handler.out.prepareStage();
        workToDo = 3;
        std::cout << ("\033[37m_END-TRACKMOTOR GOTO is not done now, has to be done again_\033[0m") << std::endl;
        EndTrackMotor.in.GoTo(commPickUp, portEndTrack, 30, degEndTrackUnloadStage, brake);
      }
    }
    return;
  }

  void Handler::unloadBallFromStagePass()
  {
    if (workToDo == 4)
    {
        std::cout << ("\033[31m_unloadBallFromStagePass_\033[0m") << std::endl;

      EndTrackMotor.in.GoTo(commPickUp, portEndTrack, 30, degEndTrackUnloadStage, brake);
      trolleyMotor.in.GoTo(commFeed, portTrolleyM, (pow+20), degTrolleyUnloadStagePass, brake);
      workToDo = 6;
      c = connState::PickUp;
    }
    return;
  }

  void Handler::unloadBallFromStageFail()
  {
    if (workToDo == 5)
    {
        std::cout << ("\033[31m_unloadBallFromStageFail_\033[0m") << std::endl;
      EndTrackMotor.in.GoTo(commPickUp, portEndTrack, 30, degEndTrackUnloadStage, brake);
      trolleyMotor.in.GoTo(commFeed, portTrolleyM, (pow+20), degTrolleyUnloadStageFail, brake);
      workToDo = 7;
      c = connState::PickUp;
    }
    return;
  }

  void Handler::dropBallToConveyerPass()
  {
    if (workToDo == 6)
    {
        std::cout << ("\033[31m_dropBallToConveyerPass_\033[0m") << std::endl;
      handler.out.unLoaded();
      trolleyMotor.in.GoTo(commFeed, portTrolleyM, (pow+20), degTrolleyDeliverToConveyerPass, brake);
      EndTrackMotor.in.GoTo(commPickUp, portEndTrack, 30, degEndTrackDeliverToConveyer, brake);
      c = connState::Drop;
    }
    return;
  }

  void Handler::dropBallToConveyerFail()
  {
    if (workToDo == 7)
    {
      std::cout << ("\033[31m_dropBallToConveyerFail_\033[0m") << std::endl;
      handler.out.unLoaded();
      trolleyMotor.in.GoTo(commFeed, portTrolleyM, (pow+20), degTrolleyDeliverToConveyerFail, brake);
      EndTrackMotor.in.GoTo(commPickUp, portEndTrack, 30, degEndTrackDeliverToConveyer, brake);
      c = connState::Drop;
    }
    return;
  }

  void Handler::goBackToInputPos()
  {
    goBackToInputPosInt = goBackToInputPosInt + 1;
    if (goBackToInputPosInt > 1)
    {
      goBackToInputPosInt = 0;
      if ((workToDo == 6 or workToDo == 7))
      {
        std::cout << ("\033[31m_goBackToInputPos_\033[0m") << std::endl;
        handler.out.startConveyor();
        ExtenderHomeSwitchSensor.in.activate();
        trolleyMotor.in.Forward(commFeed, portTrolleyM, pow);
        trackLeftSensor.in.activate();
        EndTrackMotor.in.Reverse(commPickUp, portEndTrack, 30);
        trackCalibrated = (false);
        trolleyCalibrated = (false);
        c = connState::Calibrating;
        workToDo = 1;
      }
    }
    return;
  }

}
