#ifndef DEZYNE_IMAIN_HH
#define DEZYNE_IMAIN_HH

#include <boost/bind.hpp>
#include <boost/function.hpp>
#include "lego_usb.hh"

namespace dezyne
{
  struct IMain
  {

    struct
    {
      boost::function<void (lego::USB::Device* commpickup, lego::USB::Device* commfeed, lego::USB::Device* commstage, lego::USB::Device* commconveyor, struct initConveyor* acceptstruct, struct initConveyor* rejectstruct)> init;

      struct
      {
        const char* component;
        const char* port;
        void*       address;
      } meta;
    } in;

    struct
    {

      struct
      {
        const char* component;
        const char* port;
        void*       address;
      } meta;
    } out;
  };

  inline void connect (IMain& provided, IMain& required)
  {
    assert (not required.in.init);


    provided.out = required.out;
    required.in = provided.in;
  }
}
#endif // DEZYNE_IMAIN_HH
