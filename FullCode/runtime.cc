#include "runtime.hh"

namespace dezyne {
runtime::runtime(){}

bool& runtime::handling(void* scope)
{
  return queues[scope].first;
}

void runtime::flush(void* scope)
{
  std::map<void*, std::pair<bool, std::queue<boost::function<void()> > > >& qs = queues;
  std::map<void*, std::pair<bool, std::queue<boost::function<void()> > > >::iterator it = qs.find(scope);
  if(it != qs.end())
  {
    std::queue<boost::function<void()> >& q = it->second.second;
    while(not q.empty())
    {
      boost::function<void()> event = q.front();
      q.pop();
      event();	
    }
  }
}

void runtime::defer(void* scope, const boost::function<void()>& event)
{
  auto it = std::find_if(queues.begin(), queues.end(), [](const std::pair<void*, std::pair<bool, std::queue<boost::function<void()>>>>& p){ return p.second.first;});
  if(it == queues.end())
  {
    event();
  }
  else
  {
    queues[scope].second.push(event);
  }
}

void runtime::handle_event(void* scope, const boost::function<void()>& event)
{
  bool& handle = handling(scope);
  if(not handle)
  {
    {
      scoped_value<bool> sv(handle, true);
      event();
    }
    flush(scope);
  }
  else
  {
    defer(scope, event);
  }
}
}
