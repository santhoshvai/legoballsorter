#ifndef DEZYNE_ICONVEYOR_HH
#define DEZYNE_ICONVEYOR_HH

#include <boost/bind.hpp>
#include <boost/function.hpp>
#include "lego_usb.hh"

namespace dezyne
{
  struct iConveyor
  {

    struct
    {
      boost::function<void ()> eject;
      boost::function<void (struct initConveyor* initConveyor)> init;

      struct
      {
        const char* component;
        const char* port;
        void*       address;
      } meta;
    } in;

    struct
    {

      struct
      {
        const char* component;
        const char* port;
        void*       address;
      } meta;
    } out;
  };

  inline void connect (iConveyor& provided, iConveyor& required)
  {
    assert (not required.in.eject);
    assert (not required.in.init);


    provided.out = required.out;
    required.in = provided.in;
  }
}
#endif // DEZYNE_ICONVEYOR_HH
