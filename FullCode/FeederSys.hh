#ifndef DEZYNE_FEEDERSYS_HH
#define DEZYNE_FEEDERSYS_HH

#include "Feeder.hh"
#include "ActiveMotor.hh"
#include "ActiveMotor.hh"
#include "ActiveSensor.hh"
#include "ActiveSensor.hh"


#include "iFeeder.hh"


namespace dezyne
{
  struct locator;
}

namespace dezyne
{
  struct FeederSys
  {
    dezyne::meta meta;
    Feeder feederComp;
    ActiveMotor ballsMotor;
    ActiveMotor beltMotor;
    ActiveSensor ballsSensor;
    ActiveSensor beltSensor;

    iFeeder& port;

    FeederSys(const dezyne::locator&);
  };
}
#endif // DEZYNE_FEEDERSYS_HH
