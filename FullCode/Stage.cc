#include "Stage.hh"

#include "locator.hh"
#include "runtime.hh"

#include <iostream>

namespace dezyne
{
  Stage::Stage(const locator& dezyne_locator)
  : rt(dezyne_locator.get<runtime>())
  , calibrated(0)
  , ballBool(false)
  , ballYHomeBool(false)
  , commStage()
  , commFeed()
  , commConveyor()
  , portstageYHomeSensor(IN_3)
  , portstageYEndSensor(IN_2)
  , portlightSensor(IN_1)
  , portstageXHomeSensor(IN_2)
  , portstageXEndSensor(IN_1)
  , portstageXMotor(OUT_A)
  , portstageYMotor(OUT_C)
  , pow(40)
  , degstageReceiveYMotor(Position::Inspector::ReceiveY)
  , degstageReceiveXMotor(Position::Inspector::ReceiveX)
  , degstageInspectYMotor(Position::Inspector::InspectY)
  , degstageInspectXMotor(Position::Inspector::InspectX)
  , degstageAcceptYMotor(Position::Inspector::AcceptY)
  , degstageAcceptXMotor(Position::Inspector::AcceptX)
  , degstageRejectYMotor(Position::Inspector::RejectY)
  , degstageRejectXMotor(Position::Inspector::RejectX)
  , rel(false)
  , brake(true)
  , c(connState::Initialising)
  , stage()
  , stageXMotor()
  , stageYMotor()
  , stageYHomeSensor()
  , stageYEndSensor()
  , stageXHomeSensor()
  , stageXEndSensor()
  , lightSensor()
  {
    stage.in.meta.component = "Stage";
    stage.in.meta.port = "stage";
    stage.in.meta.address = this;
    stageXMotor.out.meta.component = "Stage";
    stageXMotor.out.meta.port = "stageXMotor";
    stageXMotor.out.meta.address = this;
    stageYMotor.out.meta.component = "Stage";
    stageYMotor.out.meta.port = "stageYMotor";
    stageYMotor.out.meta.address = this;
    stageYHomeSensor.out.meta.component = "Stage";
    stageYHomeSensor.out.meta.port = "stageYHomeSensor";
    stageYHomeSensor.out.meta.address = this;
    stageYEndSensor.out.meta.component = "Stage";
    stageYEndSensor.out.meta.port = "stageYEndSensor";
    stageYEndSensor.out.meta.address = this;
    stageXHomeSensor.out.meta.component = "Stage";
    stageXHomeSensor.out.meta.port = "stageXHomeSensor";
    stageXHomeSensor.out.meta.address = this;
    stageXEndSensor.out.meta.component = "Stage";
    stageXEndSensor.out.meta.port = "stageXEndSensor";
    stageXEndSensor.out.meta.address = this;
    lightSensor.out.meta.component = "Stage";
    lightSensor.out.meta.port = "lightSensor";
    lightSensor.out.meta.address = this;

    stage.in.init = connect<lego::USB::Device*,lego::USB::Device*,lego::USB::Device*>(rt, this,
    boost::function<void(lego::USB::Device*,lego::USB::Device*,lego::USB::Device*)>
    ([this] (lego::USB::Device* commstage, lego::USB::Device* commfeed, lego::USB::Device* commconveyor)
    {
      trace (stage, "init");
      stage_init(commstage,commfeed,commconveyor);
      trace_return (stage, "return");
      return;
    }
    ));
    stage.in.prepareStage = connect<void>(rt, this,
    boost::function<void()>
    ([this] ()
    {
      trace (stage, "prepareStage");
      stage_prepareStage();
      trace_return (stage, "return");
      return;
    }
    ));
    stage.in.unLoaded = connect<void>(rt, this,
    boost::function<void()>
    ([this] ()
    {
      trace (stage, "unLoaded");
      stage_unLoaded();
      trace_return (stage, "return");
      return;
    }
    ));
    stageXMotor.out.taskPerformed=  [this] () {
      trace (stageXMotor, "taskPerformed");
      rt.defer (stageXMotor.in.meta.address, connect<void>(rt, this,
      boost::function<void()>(
      [=]
      {
        stageXMotor_taskPerformed();
        return;
      }
      )));};
    stageYMotor.out.taskPerformed=  [this] () {
      trace (stageYMotor, "taskPerformed");
      rt.defer (stageYMotor.in.meta.address, connect<void>(rt, this,
      boost::function<void()>(
      [=]
      {
        stageYMotor_taskPerformed();
        return;
      }
      )));};
    stageYHomeSensor.out.sOpened=  [this] () {
      trace (stageYHomeSensor, "sOpened");
      rt.defer (stageYHomeSensor.in.meta.address, connect<void>(rt, this,
      boost::function<void()>(
      [=]
      {
        stageYHomeSensor_sOpened();
        return;
      }
      )));};
    stageYHomeSensor.out.sClosed=  [this] () {
      trace (stageYHomeSensor, "sClosed");
      rt.defer (stageYHomeSensor.in.meta.address, connect<void>(rt, this,
      boost::function<void()>(
      [=]
      {
        stageYHomeSensor_sClosed();
        return;
      }
      )));};
    stageYEndSensor.out.sOpened=  [this] () {
      trace (stageYEndSensor, "sOpened");
      rt.defer (stageYEndSensor.in.meta.address, connect<void>(rt, this,
      boost::function<void()>(
      [=]
      {
        stageYEndSensor_sOpened();
        return;
      }
      )));};
    stageYEndSensor.out.sClosed=  [this] () {
      trace (stageYEndSensor, "sClosed");
      rt.defer (stageYEndSensor.in.meta.address, connect<void>(rt, this,
      boost::function<void()>(
      [=]
      {
        stageYEndSensor_sClosed();
        return;
      }
      )));};
    stageXHomeSensor.out.sOpened=  [this] () {
      trace (stageXHomeSensor, "sOpened");
      rt.defer (stageXHomeSensor.in.meta.address, connect<void>(rt, this,
      boost::function<void()>(
      [=]
      {
        stageXHomeSensor_sOpened();
        return;
      }
      )));};
    stageXHomeSensor.out.sClosed=  [this] () {
      trace (stageXHomeSensor, "sClosed");
      rt.defer (stageXHomeSensor.in.meta.address, connect<void>(rt, this,
      boost::function<void()>(
      [=]
      {
        stageXHomeSensor_sClosed();
        return;
      }
      )));};
    stageXEndSensor.out.sOpened=  [this] () {
      trace (stageXEndSensor, "sOpened");
      rt.defer (stageXEndSensor.in.meta.address, connect<void>(rt, this,
      boost::function<void()>(
      [=]
      {
        stageXEndSensor_sOpened();
        return;
      }
      )));};
    stageXEndSensor.out.sClosed=  [this] () {
      trace (stageXEndSensor, "sClosed");
      rt.defer (stageXEndSensor.in.meta.address, connect<void>(rt, this,
      boost::function<void()>(
      [=]
      {
        stageXEndSensor_sClosed();
        return;
      }
      )));};
    lightSensor.out.redFail=  [this] () {
      trace (lightSensor, "redFail");
      rt.defer (lightSensor.in.meta.address, connect<void>(rt, this,
      boost::function<void()>(
      [=]
      {
        lightSensor_redFail();
        return;
      }
      )));};
    lightSensor.out.bluePass=  [this] () {
      trace (lightSensor, "bluePass");
      rt.defer (lightSensor.in.meta.address, connect<void>(rt, this,
      boost::function<void()>(
      [=]
      {
        lightSensor_bluePass();
        return;
      }
      )));};
  }

  void Stage::stage_init(lego::USB::Device* commstage, lego::USB::Device* commfeed, lego::USB::Device* commconveyor)
  {
    if (c == connState::Initialising)
    {
      {
        commStage = commstage;
        commFeed = commfeed;
        commConveyor = commconveyor;
        stageYHomeSensor.in.setTouch(commStage, portstageYHomeSensor);
        stageYEndSensor.in.setTouch(commStage, portstageYEndSensor);
        stageXHomeSensor.in.setTouch(commConveyor, portstageXHomeSensor);
        stageXEndSensor.in.setTouch(commFeed, portstageXEndSensor);
        lightSensor.in.SetTouch(commStage, portlightSensor);
        calibrate ();
      }
    }
    else if (not (c == connState::Initialising))
    {
      {
      }
    }
  }

  void Stage::stage_prepareStage()
  {
      std::cout << ("\033[32m_stage_prepareStage\033[0m") << std::endl;
    if (c == connState::Busy)
    {
      {
        stageYMotor.in.GoTo(commStage, portstageYMotor, pow, degstageInspectYMotor, brake);
        stageXMotor.in.GoTo(commStage, portstageXMotor, pow, degstageInspectXMotor, brake);
        c = connState::Active;
      }
    }
    else if (c == connState::Active)
    {
      {
        stageYMotor.in.GoTo(commStage, portstageYMotor, pow, degstageInspectYMotor, brake);
        stageXMotor.in.GoTo(commStage, portstageXMotor, pow, degstageInspectXMotor, brake);
      }
    }
    else if (not (c == connState::Busy))
    {
    }
  }

  void Stage::stage_unLoaded()
  {
    if (c == connState::Inspected)
    {
      {
          std::cout << ("\033[32m_stage_unLoaded\033[0m") << std::endl;
        stageYMotor.in.GoTo(commStage, portstageYMotor, pow, degstageReceiveYMotor, brake);
        stageXMotor.in.GoTo(commStage, portstageXMotor, pow, degstageReceiveXMotor, brake);
        c = connState::unLoaded;
      }
    }
    else if (not (c == connState::Inspected))
    {
        std::cout << ("\033[32m_stage_unLoaded_NOT WORKING\033[0m") << std::endl;
    }
  }

  void Stage::stageXMotor_taskPerformed()
  {

      std::cout << ("\033[31m_stageXMotor_taskPerformed_\033[0m") << std::endl;
    if (c == connState::Initialising)
    {
      assert(false);
    }
    else if (c == connState::Inspected)
    {
        if(ballBool) stage.out.ballPass();
        else stage.out.ballFail();
        return;
      {

        if (ballBool and ballYHomeBool)
        {
          /*stage.out.ballPass();*/
          ballYHomeBool = false;
        }
        else if (ballBool and not (ballYHomeBool))   ballYHomeBool = true;
        else  {/*stage.out.ballFail();*/}
      }
    }
    else if (c == connState::unLoaded)
    c = connState::Active;
    else if (not ((c == connState::Initialising or (c == connState::unLoaded or c == connState::Inspected))))
    {
    }
  }

  void Stage::stageYMotor_taskPerformed()
  {
      std::cout << ("\033[31m_stageYMotor_taskPerformed_\033[0m") << std::endl;
    if (c == connState::Initialising)
    {
      assert(false);
    }
    else if (c == connState::Active)
    {
      {
        std::cout << ("\033[35m_LIGHT_SENSOR_CHECKED_\033[0m") << std::endl;
        lightSensor.in.GetValue(commStage, portlightSensor);
      }
    }
    else if (c == connState::Inspected)
    {
        return;
      {
        if (ballYHomeBool)
        {
         // stage.out.ballPass();
          ballYHomeBool = false;
        }
        else ballYHomeBool = true;
      }
    }
    else if (not ((c == connState::Initialising or (c == connState::Inspected or c == connState::Active))))
    {
    }
  }

  void Stage::stageYHomeSensor_sOpened()
  {
    if (c == connState::Initialising)
    {
      assert(false);
    }
    else if (not (c == connState::Initialising))
    {
      {
      }
    }
  }

  void Stage::stageYHomeSensor_sClosed()
  {
    if (c == connState::Initialising)
    {
      assert(false);
    }
    else if (c == connState::Busy)
    {
      {
        calibrated = calibrated + 1;
        stageYMotor.in.Stop(commStage, portstageYMotor, brake);
        stageYHomeSensor.in.stopMonitor();
        stageYMotor.in.ResetCount(commStage, portstageYMotor, rel);
        if (calibrated > 1)
        {
            std::cout << ("\033[32m_Receive-stage_\033[0m") << std::endl;
          stageYMotor.in.GoTo(commStage, portstageYMotor, pow, degstageReceiveYMotor, brake);
          stageXMotor.in.GoTo(commStage, portstageXMotor, pow, degstageReceiveXMotor, brake);
          c = connState::Active;
        }
      }
    }
    else if (not ((c == connState::Initialising or c == connState::Busy)))
    {
      {
      }
    }
  }

  void Stage::stageYEndSensor_sOpened()
  {
    if (c == connState::Initialising)
    {
      assert(false);
    }
    else if (not (c == connState::Initialising))
    {
      {
      }
    }
  }

  void Stage::stageYEndSensor_sClosed()
  {
    if (c == connState::Initialising)
    {
      assert(false);
    }
    else if (not (c == connState::Initialising))
    {
      {
      }
    }
  }

  void Stage::stageXHomeSensor_sOpened()
  {
    if (c == connState::Initialising)
    {
      assert(false);
    }
    else if (not (c == connState::Initialising))
    {
      {
      }
    }
  }

  void Stage::stageXHomeSensor_sClosed()
  {
    if (c == connState::Initialising)
    {
      assert(false);
    }
    else if (c == connState::Busy)
    {
      {
        calibrated = calibrated + 1;
        stageXMotor.in.Stop(commStage, portstageXMotor, brake);
        stageXHomeSensor.in.stopMonitor();
        stageXMotor.in.ResetCount(commStage, portstageXMotor, rel);
        if (calibrated > 1)
        {
            std::cout << ("\033[32m_Receive-stage_\033[0m") << std::endl;
          stageYMotor.in.GoTo(commStage, portstageYMotor, pow, degstageReceiveYMotor, brake);
          stageXMotor.in.GoTo(commStage, portstageXMotor, pow, degstageReceiveXMotor, brake);
        }
      }
    }
    else if (not ((c == connState::Initialising or c == connState::Busy)))
    {
      {
      }
    }
  }

  void Stage::stageXEndSensor_sOpened()
  {
    if (c == connState::Initialising)
    {
      assert(false);
    }
    else if (not (c == connState::Initialising))
    {
      {
      }
    }
  }

  void Stage::stageXEndSensor_sClosed()
  {
    if (c == connState::Initialising)
    {
      assert(false);
    }
    else if (not (c == connState::Initialising))
    {
      {
      }
    }
  }

  void Stage::lightSensor_redFail()
  {
    if (c == connState::Active)
    {
      {
         std::cout << ("\033[31m_lightSensor_redFail_\033[0m") << std::endl;
        stageYMotor.in.GoTo(commStage, portstageYMotor, pow, degstageRejectYMotor, brake);
        stageXMotor.in.GoTo(commStage, portstageXMotor, pow, degstageRejectXMotor, brake);
        ballBool = false;
        c = connState::Inspected;
      }
    }
    else if (not (c == connState::Active))
    {
      {
        assert(false);
      }
    }
  }

  void Stage::lightSensor_bluePass()
  {
    if (c == connState::Active)
    {
        std::cout << ("\034[31m_lightSensor_bluePass_\033[0m") << std::endl;
      {
        stageYMotor.in.GoTo(commStage, portstageYMotor, pow, degstageAcceptYMotor, brake);
        stageXMotor.in.GoTo(commStage, portstageXMotor, pow, degstageAcceptXMotor, brake);
        ballBool = true;
        c = connState::Inspected;
      }
    }
    else if (not (c == connState::Active))
    {
      {
        assert(false);
      }
    }
  }

  void Stage::calibrate()
  {
    calibrated = 0;
    stageXHomeSensor.in.activate();
    stageYHomeSensor.in.activate();
    stageXMotor.in.Reverse(commStage, portstageXMotor, (pow-10));
    stageYMotor.in.Reverse(commStage, portstageYMotor, (pow-10));
    c = connState::Busy;
    return;
  }

}
