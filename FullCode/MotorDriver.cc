#include "MotorDriver.hh"

#include "locator.hh"
#include "runtime.hh"

#include <iostream>
#include <cstdlib>

namespace dezyne
{
  MotorDriver::MotorDriver(const locator& dezyne_locator)
  : rt(dezyne_locator.get<runtime>())
  , portM()
  , powM()
  , degM()
  , commM()
  , relM()
  , brakeM()
  , timeoutBool(false)
  , state(States::Stopped)
  , motor1()
  , timer2()
  , client1()
  {
    client1.in.meta.component = "MotorDriver";
    client1.in.meta.port = "client1";
    client1.in.meta.address = this;
    motor1.out.meta.component = "MotorDriver";
    motor1.out.meta.port = "motor1";
    motor1.out.meta.address = this;
    timer2.out.meta.component = "MotorDriver";
    timer2.out.meta.port = "timer2";
    timer2.out.meta.address = this;

    client1.in.RotationCount = connect<lego::USB::Device*,int>(rt, this,
    boost::function<void(lego::USB::Device*,int)>
    ([this] (lego::USB::Device* comm, int port)
    {
      trace (client1, "RotationCount");
      client1_RotationCount(comm,port);
      trace_return (client1, "return");
      return;
    }
    ));
    client1.in.ResetCount = connect<lego::USB::Device*,int,bool>(rt, this,
    boost::function<void(lego::USB::Device*,int,bool)>
    ([this] (lego::USB::Device* comm, int port, bool relative)
    {
      trace (client1, "ResetCount");
      client1_ResetCount(comm,port,relative);
      trace_return (client1, "return");
      return;
    }
    ));
    client1.in.Forward = connect<lego::USB::Device*,int,int>(rt, this,
    boost::function<void(lego::USB::Device*,int,int)>
    ([this] (lego::USB::Device* comm, int port, int power)
    {
      trace (client1, "Forward");
      client1_Forward(comm,port,power);
      trace_return (client1, "return");
      return;
    }
    ));
    client1.in.Reverse = connect<lego::USB::Device*,int,int>(rt, this,
    boost::function<void(lego::USB::Device*,int,int)>
    ([this] (lego::USB::Device* comm, int port, int power)
    {
      trace (client1, "Reverse");
      client1_Reverse(comm,port,power);
      trace_return (client1, "return");
      return;
    }
    ));
    client1.in.Stop = connect<lego::USB::Device*,int,bool>(rt, this,
    boost::function<void(lego::USB::Device*,int,bool)>
    ([this] (lego::USB::Device* comm, int port, bool brake)
    {
      trace (client1, "Stop");
      client1_Stop(comm,port,brake);
      trace_return (client1, "return");
      return;
    }
    ));
    client1.in.GoTo = connect<lego::USB::Device*,int,int,int,bool>(rt, this,
    boost::function<void(lego::USB::Device*,int,int,int,bool)>
    ([this] (lego::USB::Device* comm, int port, int power, int tacho, bool brake)
    {
      trace (client1, "GoTo");
      client1_GoTo(comm,port,power,tacho,brake);
      trace_return (client1, "return");
      return;
    }
    ));
    motor1.out.Count=  [this] (int count) {
      std::cout << "##########" << std::endl;
      //trace (motor1, "Count");
      rt.defer (motor1.in.meta.address, connect<void>(rt, this,
      boost::function<void()>(
      [=]
      {
          std::cout << "***********" << std::endl;
        motor1_Count(count);
        return;
      }
      )));};
    timer2.out.timeout=  [this] () {
      trace (timer2, "timeout");
      rt.defer (timer2.in.meta.address, connect<void>(rt, this,
      boost::function<void()>(
      [=]
      {
        timer2_timeout();
        return;
      }
      )));};
  }

  void MotorDriver::motor1_Count(int count)
  {
     std::cout << "COUNT CALLED it works : " << count << std::endl;
    if (state == States::Stopped)
    {
      {
      }
    }
    else if (state == States::Running)
    {
      {
      }
    }
    else if (state == States::Moving)
    {
      {
        if (timeoutBool)
        {
          int rotationCount = NXT::Motor::GetRotationCount(commM, portM);
          bool greaterBool = greater (rotationCount, degM);
          if (greaterBool)
          {
            std::cout << "Stopped after GOTO on PORT: " << portM << std::endl;
            client1.out.taskPerformed();
            timer2.in.cancel();
            state = States::Stopped;
          }
          else
          timer2.in.create();
          timeoutBool = false;
        }
      }
    }
  }

  void MotorDriver::timer2_timeout()
  {
    if (state == States::Stopped)
    {
      assert(false);
    }
    else if (state == States::Running)
    {
      assert(false);
    }
    else if (state == States::Moving)
    {
      {
        motor1.in.GetRotationCount(commM, portM);
        timeoutBool = true;
        if (timeoutBool)
        {
          int rotationCount = NXT::Motor::GetRotationCount(commM, portM);
          bool greaterBool = greater (rotationCount, degM);
          if (greaterBool)
          {
            std::cout << "Stopped after GOTO on PORT: " << portM << std::endl;
            client1.out.taskPerformed();
            timer2.in.cancel();
            state = States::Stopped;
          }
          else
          timer2.in.create();
          timeoutBool = false;
        }
      }
    }
  }

  void MotorDriver::client1_RotationCount(lego::USB::Device* comm, int port)
  {
    if (state == States::Stopped)
    {
      {
        commM = comm;
        portM = port;
        motor1.in.GetRotationCount(commM, portM);
      }
    }
    else if (state == States::Running)
    {
      {
        commM = comm;
        portM = port;
        motor1.in.GetRotationCount(commM, portM);
      }
    }
    else if (state == States::Moving)
    {
      {
        commM = comm;
        portM = port;
        motor1.in.GetRotationCount(commM, portM);
      }
    }
  }

  void MotorDriver::client1_ResetCount(lego::USB::Device* comm, int port, bool relative)
  {
    if (state == States::Stopped)
    {
      {
        bool rel = relative;
        {
          commM = comm;
          portM = port;
          relM = rel;
          motor1.in.ResetRotationCount(commM, portM, relM);
        }
      }
    }
    else if (state == States::Running)
    {
      {
        bool rel = relative;
        {
          commM = comm;
          portM = port;
          relM = rel;
          motor1.in.ResetRotationCount(commM, portM, relM);
        }
      }
    }
    else if (state == States::Moving)
    {
      {
        bool rel = relative;
        {
          commM = comm;
          portM = port;
          relM = rel;
          motor1.in.ResetRotationCount(commM, portM, relM);
        }
      }
    }
  }

  void MotorDriver::client1_Forward(lego::USB::Device* comm, int port, int power)
  {
    if (state == States::Stopped)
    {
      {
        int pow = power;
        {
          commM = comm;
          portM = port;
          powM = pow;
          motor1.in.SetForward(commM, portM, powM);
          state = States::Running;
        }
      }
    }
    else if (state == States::Running)
    {
      {
        int pow = power;
        {
          commM = comm;
          portM = port;
          powM = pow;
          motor1.in.SetForward(commM, portM, powM);
          state = States::Running;
        }
      }
    }
    else if (state == States::Moving)
    {
      {
      }
    }
  }

  void MotorDriver::client1_Reverse(lego::USB::Device* comm, int port, int power)
  {
    if (state == States::Stopped)
    {
      {
        int pow = power;
        {
          commM = comm;
          portM = port;
          powM = pow;
          motor1.in.SetReverse(commM, portM, powM);
          state = States::Running;
        }
      }
    }
    else if (state == States::Running)
    {
      {
        int pow = power;
        {
          commM = comm;
          portM = port;
          powM = pow;
          motor1.in.SetReverse(commM, portM, powM);
          state = States::Running;
        }
      }
    }
    else if (state == States::Moving)
    {
      {
      }
    }
  }

  void MotorDriver::client1_Stop(lego::USB::Device* comm, int port, bool brake)
  {
    if (state == States::Stopped)
    {
      {
        commM = comm;
        portM = port;
        brakeM = brake;
        motor1.in.Stop(commM, portM, brakeM);
      }
    }
    else if (state == States::Running)
    {
      {
        commM = comm;
        portM = port;
        brakeM = brake;
        motor1.in.Stop(commM, portM, brakeM);
        state = States::Stopped;
      }
    }
    else if (state == States::Moving)
    {
      {
        commM = comm;
        portM = port;
        brakeM = brake;
        motor1.in.Stop(commM, portM, brakeM);
        timer2.in.cancel();
        state = States::Stopped;
      }
    }
  }

  void MotorDriver::client1_GoTo(lego::USB::Device* comm, int port, int power, int tacho, bool brake)
  {
    if (state == States::Stopped)
    {
      {
        int pow = power;
        int deg = tacho;
        {
          commM = comm;
          portM = port;
          powM = pow;
          degM = deg;
          brakeM = brake;
          motor1.in.GoTo(commM, portM, powM, degM, brakeM);
          timer2.in.create();
          state = States::Moving;
        }
      }
    }
    else if (state == States::Running)
    {
      {
        int pow = power;
        int deg = tacho;
        {
          commM = comm;
          portM = port;
          powM = pow;
          degM = deg;
          brakeM = brake;
          motor1.in.GoTo(commM, portM, powM, degM, brakeM);
          timer2.in.create();
          state = States::Moving;
        }
      }
    }
    else if (state == States::Moving)
    {
      {
      }
    }
  }

  bool MotorDriver::greater(int rotationCount, int degM)
  {
    // std::cout << "\033[33m" << rotationCount << " >= " << degM << "\033[0m" << std::endl;
    //  if(std::abs(degM) == 50) return (std::abs(rotationCount) >= std::abs(degM-10));
    // std::cout << "\033[33m" << rotationCount << " >= " << degM << "\033[0m" << std::endl;
    // std::cout << "TASK PERFORMED CHECK: " << " degM: " << degM << " r: " << rotationCount << " abs: "<< (std::abs(degM) - std::abs(rotationCount)) << std::endl;
    return( std::abs(std::abs(degM) - std::abs(rotationCount)) <= 9 );
    //return (std::abs(rotationCount) >= std::abs(degM));
  }

}
