#ifndef DEZYNE_IACTIVESENSOR_HH
#define DEZYNE_IACTIVESENSOR_HH

#include <boost/bind.hpp>
#include <boost/function.hpp>
#include "lego_usb.hh"

namespace dezyne
{
  struct iActiveSensor
  {

    struct
    {
      boost::function<void (lego::USB::Device* comm, int port)> setTouch;
      boost::function<void ()> activate;
      boost::function<void ()> stopMonitor;

      struct
      {
        const char* component;
        const char* port;
        void*       address;
      } meta;
    } in;

    struct
    {
      boost::function<void ()> sOpened;
      boost::function<void ()> sClosed;

      struct
      {
        const char* component;
        const char* port;
        void*       address;
      } meta;
    } out;
  };

  inline void connect (iActiveSensor& provided, iActiveSensor& required)
  {
    assert (not required.in.setTouch);
    assert (not required.in.activate);
    assert (not required.in.stopMonitor);

    assert (not provided.out.sOpened);
    assert (not provided.out.sClosed);

    provided.out = required.out;
    required.in = provided.in;
  }
}
#endif // DEZYNE_IACTIVESENSOR_HH
