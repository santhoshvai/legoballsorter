#include "Controller.hh"

#include "locator.hh"
#include "runtime.hh"

#include <iostream>

namespace dezyne
{
  Controller::Controller(const locator& dezyne_locator)
  : rt(dezyne_locator.get<runtime>())
  , ballBool(false)
  , commFeed()
  , commPickUp()
  , commConveyor()
  , commStage()
  , acceptStruct()
  , rejectStruct()
  , c(connState::Initialising)
  , port()
  , handler()
  , stage()
  , accept()
  , reject()
  , feeder()
  {
    port.in.meta.component = "Controller";
    port.in.meta.port = "port";
    port.in.meta.address = this;
    handler.out.meta.component = "Controller";
    handler.out.meta.port = "handler";
    handler.out.meta.address = this;
    stage.out.meta.component = "Controller";
    stage.out.meta.port = "stage";
    stage.out.meta.address = this;
    accept.out.meta.component = "Controller";
    accept.out.meta.port = "accept";
    accept.out.meta.address = this;
    reject.out.meta.component = "Controller";
    reject.out.meta.port = "reject";
    reject.out.meta.address = this;
    feeder.out.meta.component = "Controller";
    feeder.out.meta.port = "feeder";
    feeder.out.meta.address = this;

    port.in.init = connect<lego::USB::Device*,lego::USB::Device*,lego::USB::Device*,lego::USB::Device*,struct initConveyor*,struct initConveyor*>(rt, this,
    boost::function<void(lego::USB::Device*,lego::USB::Device*,lego::USB::Device*,lego::USB::Device*,struct initConveyor*,struct initConveyor*)>
    ([this] (lego::USB::Device* commpickup, lego::USB::Device* commfeed, lego::USB::Device* commstage, lego::USB::Device* commconveyor, struct initConveyor* acceptstruct, struct initConveyor* rejectstruct)
    {
      trace (port, "init");
      port_init(commpickup,commfeed,commstage,commconveyor,acceptstruct,rejectstruct);
      trace_return (port, "return");
      return;
    }
    ));
    handler.out.prepareStage=  [this] () {
      trace (handler, "prepareStage");
      rt.defer (handler.in.meta.address, connect<void>(rt, this,
      boost::function<void()>(
      [=]
      {
        handler_prepareStage();
        return;
      }
      )));};
    handler.out.unLoaded=  [this] () {
      trace (handler, "unLoaded");
      rt.defer (handler.in.meta.address, connect<void>(rt, this,
      boost::function<void()>(
      [=]
      {
        handler_unLoaded();
        return;
      }
      )));};
    handler.out.startConveyor=  [this] () {
      trace (handler, "startConveyor");
      rt.defer (handler.in.meta.address, connect<void>(rt, this,
      boost::function<void()>(
      [=]
      {
        handler_startConveyor();
        return;
      }
      )));};
    stage.out.ballPass=  [this] () {
      trace (stage, "ballPass");
      rt.defer (stage.in.meta.address, connect<void>(rt, this,
      boost::function<void()>(
      [=]
      {
        stage_ballPass();
        return;
      }
      )));};
    stage.out.ballFail=  [this] () {
      trace (stage, "ballFail");
      rt.defer (stage.in.meta.address, connect<void>(rt, this,
      boost::function<void()>(
      [=]
      {
        stage_ballFail();
        return;
      }
      )));};
    feeder.out.ballFeeded=  [this] () {
      trace (feeder, "ballFeeded");
      rt.defer (feeder.in.meta.address, connect<void>(rt, this,
      boost::function<void()>(
      [=]
      {
        feeder_ballFeeded();
        return;
      }
      )));};
  }

  void Controller::port_init(lego::USB::Device* commpickup, lego::USB::Device* commfeed, lego::USB::Device* commstage, lego::USB::Device* commconveyor, struct initConveyor* acceptstruct, struct initConveyor* rejectstruct)
  {
    if (c == connState::Initialising)
    {
      commFeed = commfeed;
      commPickUp = commpickup;
      commConveyor = commconveyor;
      commStage = commstage;
      acceptStruct = acceptstruct;
      rejectStruct = rejectstruct;
      initAll ();
    }
    else if (not (c == connState::Initialising))
    {
      {
      }
    }
  }

  void Controller::handler_prepareStage()
  {
      stage.in.prepareStage(); return;
    if (c == connState::Busy)
    {
      stage.in.prepareStage();
    }
    else if (not (c == connState::Busy))
    {
      {
          std::cout << "\033[32m_Controller::handler_prepareStage NOT fired_\033[0m" << std::endl;
      }
    }
  }

  void Controller::handler_unLoaded()
  {
      stage.in.unLoaded(); return;
    if (c == connState::Busy)
    {
      stage.in.unLoaded();
    }
    else if (not (c == connState::Busy))
    {
      {
      }
    }
  }

  void Controller::handler_startConveyor()
  {
    if (c == connState::AfterUnload)
    {
      if (ballBool)
      accept.in.eject();
      else
      reject.in.eject();

      feeder.in.push();
      c = connState::Busy;
    }
    else if (not (c == connState::AfterUnload))
    {
    }
  }

  void Controller::stage_ballPass()
  {
    if (not (c == connState::Initialising))
    {
      {
        ballBool = true;
        handler.in.ballPass();
        c = connState::AfterUnload;
      }
    }
    else if (not (not (c == connState::Initialising)))
    {
      {
      }
    }
  }

  void Controller::stage_ballFail()
  {
    if (not (c == connState::Initialising))
    {
      {
        ballBool = false;
        handler.in.ballFail();
        c = connState::AfterUnload;
      }
    }
    else if (not (not (c == connState::Initialising)))
    {
      {
      }
    }
  }

  void Controller::feeder_ballFeeded()
  {
    if (c == connState::Busy)
    {
      handler.in.ballFeeded();
    }
    else if (not (c == connState::Busy))
    {
      {
      }
    }
  }

  void Controller::initAll()
  {
    handler.in.init(commPickUp, commFeed, commConveyor);
    stage.in.init(commStage, commFeed, commConveyor);
    feeder.in.init(commFeed, commPickUp);
    c = connState::Busy;
    accept.in.init(acceptStruct);
    reject.in.init(rejectStruct);

    return;
  }

}
