#include "Main.hh"

namespace dezyne
{
  Main::Main(const dezyne::locator& dezyne_locator)
  : meta{{reinterpret_cast<component*>(&MainCtrlr),reinterpret_cast<component*>(&feeder),reinterpret_cast<component*>(&stage),reinterpret_cast<component*>(&handler),reinterpret_cast<component*>(&accept),reinterpret_cast<component*>(&reject)}, 0, reinterpret_cast<component*>(this), ""}
  , MainCtrlr(dezyne_locator)
  , feeder(dezyne_locator)
  , stage(dezyne_locator)
  , handler(dezyne_locator)
  , accept(dezyne_locator)
  , reject(dezyne_locator)
  , port(MainCtrlr.port)
  {
    MainCtrlr.meta.parent = reinterpret_cast<component*>(this);
    MainCtrlr.meta.address = reinterpret_cast<component*>(&MainCtrlr);
    MainCtrlr.meta.name = "MainCtrlr";
    feeder.meta.parent = reinterpret_cast<component*>(this);
    feeder.meta.address = reinterpret_cast<component*>(&feeder);
    feeder.meta.name = "feeder";
    stage.meta.parent = reinterpret_cast<component*>(this);
    stage.meta.address = reinterpret_cast<component*>(&stage);
    stage.meta.name = "stage";
    handler.meta.parent = reinterpret_cast<component*>(this);
    handler.meta.address = reinterpret_cast<component*>(&handler);
    handler.meta.name = "handler";
    accept.meta.parent = reinterpret_cast<component*>(this);
    accept.meta.address = reinterpret_cast<component*>(&accept);
    accept.meta.name = "accept";
    reject.meta.parent = reinterpret_cast<component*>(this);
    reject.meta.address = reinterpret_cast<component*>(&reject);
    reject.meta.name = "reject";
    connect(accept.port, MainCtrlr.accept);
    connect(reject.port, MainCtrlr.reject);
    connect(feeder.port, MainCtrlr.feeder);
    connect(stage.port, MainCtrlr.stage);
    connect(handler.port, MainCtrlr.handler);
  }
}
