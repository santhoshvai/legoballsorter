#ifndef DEZYNE_CONVEYOR_HH
#define DEZYNE_CONVEYOR_HH

#include "iConveyor.hh"
#include "iActiveMotor.hh"
#include "iActiveSensor.hh"


#include "runtime.hh"

namespace dezyne
{
  struct locator;
  struct runtime;

  struct Conveyor
  {
    dezyne::meta meta;
    runtime& rt;
    struct convState
    {
      enum type
      {
        Active, Idle
      };
      static const char* to_string(type v)
      {
        switch(v)
        {
          case Active: return "convState_Active";
          case Idle: return "convState_Idle";

        }
        return "";
      }
    };
    lego::USB::Device* commConveyor;
    struct initConveyor* InitConv;
    int portexitSensor;
    int portbeltMotor;
    int pow;
    bool rel;
    bool brake;
    Conveyor::convState::type c;
    iConveyor conveyor;
    iActiveMotor beltMotor;
    iActiveSensor exitSensor;

    Conveyor(const locator&);

    private:
    void conveyor_eject();
    void conveyor_init(struct initConveyor* initConveyor);
    void beltMotor_taskPerformed();
    void exitSensor_sOpened();
    void exitSensor_sClosed();
    void init(struct initConveyor* convInit);
  };
}
#endif // DEZYNE_CONVEYOR_HH
