#ifndef DEZYNE_ACTIVEMOTOR_HH
#define DEZYNE_ACTIVEMOTOR_HH

#include "MotorDriver.hh"
#include "Motor.hh"
#include "timer.hh"


#include "iActiveMotor.hh"


namespace dezyne
{
  struct locator;
}

namespace dezyne
{
  struct ActiveMotor
  {
    dezyne::meta meta;
    MotorDriver Mdriver;
    Motor motor;
    timer Mtim;

    iActiveMotor& port;

    ActiveMotor(const dezyne::locator&);
  };
}
#endif // DEZYNE_ACTIVEMOTOR_HH
