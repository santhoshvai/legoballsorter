#include "StageSys.hh"

namespace dezyne
{
  StageSys::StageSys(const dezyne::locator& dezyne_locator)
  : meta{{reinterpret_cast<component*>(&stageComp),reinterpret_cast<component*>(&stageXMotor),reinterpret_cast<component*>(&stageYMotor),reinterpret_cast<component*>(&stageYHomeSensor),reinterpret_cast<component*>(&stageYEndSensor),reinterpret_cast<component*>(&stageXHomeSensor),reinterpret_cast<component*>(&stageXEndSensor),reinterpret_cast<component*>(&lightSensor)}, 0, reinterpret_cast<component*>(this), ""}
  , stageComp(dezyne_locator)
  , stageXMotor(dezyne_locator)
  , stageYMotor(dezyne_locator)
  , stageYHomeSensor(dezyne_locator)
  , stageYEndSensor(dezyne_locator)
  , stageXHomeSensor(dezyne_locator)
  , stageXEndSensor(dezyne_locator)
  , lightSensor(dezyne_locator)
  , port(stageComp.stage)
  {
    stageComp.meta.parent = reinterpret_cast<component*>(this);
    stageComp.meta.address = reinterpret_cast<component*>(&stageComp);
    stageComp.meta.name = "stageComp";
    stageXMotor.meta.parent = reinterpret_cast<component*>(this);
    stageXMotor.meta.address = reinterpret_cast<component*>(&stageXMotor);
    stageXMotor.meta.name = "stageXMotor";
    stageYMotor.meta.parent = reinterpret_cast<component*>(this);
    stageYMotor.meta.address = reinterpret_cast<component*>(&stageYMotor);
    stageYMotor.meta.name = "stageYMotor";
    stageYHomeSensor.meta.parent = reinterpret_cast<component*>(this);
    stageYHomeSensor.meta.address = reinterpret_cast<component*>(&stageYHomeSensor);
    stageYHomeSensor.meta.name = "stageYHomeSensor";
    stageYEndSensor.meta.parent = reinterpret_cast<component*>(this);
    stageYEndSensor.meta.address = reinterpret_cast<component*>(&stageYEndSensor);
    stageYEndSensor.meta.name = "stageYEndSensor";
    stageXHomeSensor.meta.parent = reinterpret_cast<component*>(this);
    stageXHomeSensor.meta.address = reinterpret_cast<component*>(&stageXHomeSensor);
    stageXHomeSensor.meta.name = "stageXHomeSensor";
    stageXEndSensor.meta.parent = reinterpret_cast<component*>(this);
    stageXEndSensor.meta.address = reinterpret_cast<component*>(&stageXEndSensor);
    stageXEndSensor.meta.name = "stageXEndSensor";
    lightSensor.meta.parent = reinterpret_cast<component*>(this);
    lightSensor.meta.address = reinterpret_cast<component*>(&lightSensor);
    lightSensor.meta.name = "lightSensor";
    connect(stageXMotor.port, stageComp.stageXMotor);
    connect(stageYMotor.port, stageComp.stageYMotor);
    connect(stageYHomeSensor.port, stageComp.stageYHomeSensor);
    connect(stageYEndSensor.port, stageComp.stageYEndSensor);
    connect(stageXHomeSensor.port, stageComp.stageXHomeSensor);
    connect(stageXEndSensor.port, stageComp.stageXEndSensor);
    connect(lightSensor.port, stageComp.lightSensor);
  }
}
