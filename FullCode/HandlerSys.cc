#include "HandlerSys.hh"

namespace dezyne
{
  HandlerSys::HandlerSys(const dezyne::locator& dezyne_locator)
  : meta{{reinterpret_cast<component*>(&handlerComp),reinterpret_cast<component*>(&EndTrackMotor),reinterpret_cast<component*>(&GripperMotor),reinterpret_cast<component*>(&HoistMotor),reinterpret_cast<component*>(&trolleyMotor),reinterpret_cast<component*>(&trackLeftSensor),reinterpret_cast<component*>(&pickupSensor),reinterpret_cast<component*>(&ExtenderHomeSwitchSensor),reinterpret_cast<component*>(&GripperSensor)}, 0, reinterpret_cast<component*>(this), ""}
  , handlerComp(dezyne_locator)
  , EndTrackMotor(dezyne_locator)
  , GripperMotor(dezyne_locator)
  , HoistMotor(dezyne_locator)
  , trolleyMotor(dezyne_locator)
  , trackLeftSensor(dezyne_locator)
  , pickupSensor(dezyne_locator)
  , ExtenderHomeSwitchSensor(dezyne_locator)
  , GripperSensor(dezyne_locator)
  , port(handlerComp.handler)
  {
    handlerComp.meta.parent = reinterpret_cast<component*>(this);
    handlerComp.meta.address = reinterpret_cast<component*>(&handlerComp);
    handlerComp.meta.name = "handlerComp";
    EndTrackMotor.meta.parent = reinterpret_cast<component*>(this);
    EndTrackMotor.meta.address = reinterpret_cast<component*>(&EndTrackMotor);
    EndTrackMotor.meta.name = "EndTrackMotor";
    GripperMotor.meta.parent = reinterpret_cast<component*>(this);
    GripperMotor.meta.address = reinterpret_cast<component*>(&GripperMotor);
    GripperMotor.meta.name = "GripperMotor";
    HoistMotor.meta.parent = reinterpret_cast<component*>(this);
    HoistMotor.meta.address = reinterpret_cast<component*>(&HoistMotor);
    HoistMotor.meta.name = "HoistMotor";
    trolleyMotor.meta.parent = reinterpret_cast<component*>(this);
    trolleyMotor.meta.address = reinterpret_cast<component*>(&trolleyMotor);
    trolleyMotor.meta.name = "trolleyMotor";
    trackLeftSensor.meta.parent = reinterpret_cast<component*>(this);
    trackLeftSensor.meta.address = reinterpret_cast<component*>(&trackLeftSensor);
    trackLeftSensor.meta.name = "trackLeftSensor";
    pickupSensor.meta.parent = reinterpret_cast<component*>(this);
    pickupSensor.meta.address = reinterpret_cast<component*>(&pickupSensor);
    pickupSensor.meta.name = "pickupSensor";
    ExtenderHomeSwitchSensor.meta.parent = reinterpret_cast<component*>(this);
    ExtenderHomeSwitchSensor.meta.address = reinterpret_cast<component*>(&ExtenderHomeSwitchSensor);
    ExtenderHomeSwitchSensor.meta.name = "ExtenderHomeSwitchSensor";
    GripperSensor.meta.parent = reinterpret_cast<component*>(this);
    GripperSensor.meta.address = reinterpret_cast<component*>(&GripperSensor);
    GripperSensor.meta.name = "GripperSensor";
    connect(EndTrackMotor.port, handlerComp.EndTrackMotor);
    connect(GripperMotor.port, handlerComp.GripperMotor);
    connect(HoistMotor.port, handlerComp.HoistMotor);
    connect(trolleyMotor.port, handlerComp.trolleyMotor);
    connect(GripperSensor.port, handlerComp.GripperSensor);
    connect(trackLeftSensor.port, handlerComp.trackLeftSensor);
    connect(pickupSensor.port, handlerComp.pickupSensor);
    connect(ExtenderHomeSwitchSensor.port, handlerComp.ExtenderHomeSwitchSensor);
  }
}
