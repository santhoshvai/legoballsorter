#include "ActiveMotor.hh"

namespace dezyne
{
  ActiveMotor::ActiveMotor(const dezyne::locator& dezyne_locator)
  : meta{{reinterpret_cast<component*>(&Mdriver),reinterpret_cast<component*>(&motor),reinterpret_cast<component*>(&Mtim)}, 0, reinterpret_cast<component*>(this), ""}
  , Mdriver(dezyne_locator)
  , motor(dezyne_locator)
  , Mtim(dezyne_locator)
  , port(Mdriver.client1)
  {
    Mdriver.meta.parent = reinterpret_cast<component*>(this);
    Mdriver.meta.address = reinterpret_cast<component*>(&Mdriver);
    Mdriver.meta.name = "Mdriver";
    motor.meta.parent = reinterpret_cast<component*>(this);
    motor.meta.address = reinterpret_cast<component*>(&motor);
    motor.meta.name = "motor";
    Mtim.meta.parent = reinterpret_cast<component*>(this);
    Mtim.meta.address = reinterpret_cast<component*>(&Mtim);
    Mtim.meta.name = "Mtim";
    connect(motor.port, Mdriver.motor1);
    connect(Mtim.port, Mdriver.timer2);
  }
}
