#ifndef DEZYNE_SENSORDRIVER_HH
#define DEZYNE_SENSORDRIVER_HH

#include "iTouchSensor.hh"
#include "iTimer.hh"
#include "iActiveSensor.hh"


#include "runtime.hh"

namespace dezyne
{
  struct locator;
  struct runtime;

  struct SensorDriver
  {
    dezyne::meta meta;
    runtime& rt;
    struct States
    {
      enum type
      {
        Idle, Initializing, Initialized
      };
      static const char* to_string(type v)
      {
        switch(v)
        {
          case Idle: return "States_Idle";
          case Initializing: return "States_Initializing";
          case Initialized: return "States_Initialized";

        }
        return "";
      }
    };
    int portS;
    lego::USB::Device* commS;
    iTouchSensor::SensorValue::type v_old;
    bool run;
    SensorDriver::States::type state;
    iTouchSensor::SensorValue::type reply_iTouchSensor_SensorValue;
    iTouchSensor sensor1;
    iTimer timer1;
    iActiveSensor client1;

    SensorDriver(const locator&);

    private:
    void client1_setTouch(lego::USB::Device* comm, int port);
    void client1_activate();
    void client1_stopMonitor();
    void timer1_timeout();
  };
}
#endif // DEZYNE_SENSORDRIVER_HH
