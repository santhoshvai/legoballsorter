#include "SensorDriver.hh"

#include "locator.hh"
#include "runtime.hh"

#include <iostream>

namespace dezyne
{
  SensorDriver::SensorDriver(const locator& dezyne_locator)
  : rt(dezyne_locator.get<runtime>())
  , portS()
  , commS()
  , v_old(iTouchSensor::SensorValue::Opened)
  , run(false)
  , state(States::Idle)
  , sensor1()
  , timer1()
  , client1()
  {
    client1.in.meta.component = "SensorDriver";
    client1.in.meta.port = "client1";
    client1.in.meta.address = this;
    sensor1.out.meta.component = "SensorDriver";
    sensor1.out.meta.port = "sensor1";
    sensor1.out.meta.address = this;
    timer1.out.meta.component = "SensorDriver";
    timer1.out.meta.port = "timer1";
    timer1.out.meta.address = this;

    client1.in.setTouch = connect<lego::USB::Device*,int>(rt, this,
    boost::function<void(lego::USB::Device*,int)>
    ([this] (lego::USB::Device* comm, int port)
    {
      trace (client1, "setTouch");
      client1_setTouch(comm,port);
      trace_return (client1, "return");
      return;
    }
    ));
    client1.in.activate = connect<void>(rt, this,
    boost::function<void()>
    ([this] ()
    {
      trace (client1, "activate");
      client1_activate();
      trace_return (client1, "return");
      return;
    }
    ));
    client1.in.stopMonitor = connect<void>(rt, this,
    boost::function<void()>
    ([this] ()
    {
      trace (client1, "stopMonitor");
      client1_stopMonitor();
      trace_return (client1, "return");
      return;
    }
    ));
    timer1.out.timeout=  [this] () {
      trace (timer1, "timeout");
      rt.defer (timer1.in.meta.address, connect<void>(rt, this,
      boost::function<void()>(
      [=]
      {
        timer1_timeout();
        return;
      }
      )));};
  }

  void SensorDriver::timer1_timeout()
  {
    if (state == States::Idle)
    {
      assert(false);
    }
    else if (state == States::Initializing)
    {
      assert(false);
    }
    else if (state == States::Initialized)
    {
      {
        iTouchSensor::SensorValue::type v = sensor1.in.GetValue (commS, portS);
        timer1.in.create();
        if (v != v_old)
        {
          if (v == iTouchSensor::SensorValue::Opened)
          client1.out.sOpened();
          else
          client1.out.sClosed();
          v_old = v;
        }
      }
    }
  }

  void SensorDriver::client1_setTouch(lego::USB::Device* comm, int port)
  {
    if (state == States::Idle)
    {
      {
        lego::USB::Device* c = comm;
        int p = port;
        {
          commS = c;
          portS = p;
          sensor1.in.SetTouch(commS, portS);
          state = States::Initializing;
        }
      }
    }
    else if (state == States::Initializing)
    {
      assert(false);
    }
    else if (state == States::Initialized)
    {
      assert(false);
    }
  }

  void SensorDriver::client1_activate()
  {
    if (state == States::Idle)
    {
      assert(false);
    }
    else if (state == States::Initializing)
    {
      {
        timer1.in.create();
        run = true;
        state = States::Initialized;
      }
    }
    else if (state == States::Initialized)
    {
      {
        if (not (run))
        {
          run = true;
          timer1.in.create();
        }
      }
    }
  }

  void SensorDriver::client1_stopMonitor()
  {
    if (state == States::Idle)
    {
      assert(false);
    }
    else if (state == States::Initializing)
    {
      assert(false);
    }
    else if (state == States::Initialized)
    {
      {
        timer1.in.cancel();
        run = false;
        state = States::Initializing;
      }
    }
  }


}
