#ifndef DEZYNE_MOTORDRIVER_HH
#define DEZYNE_MOTORDRIVER_HH

#include "iMotor.hh"
#include "iTimer.hh"
#include "iActiveMotor.hh"


#include "runtime.hh"

namespace dezyne
{
  struct locator;
  struct runtime;

  struct MotorDriver
  {
    dezyne::meta meta;
    runtime& rt;
    struct States
    {
      enum type
      {
        Running, Stopped, Moving
      };
      static const char* to_string(type v)
      {
        switch(v)
        {
          case Running: return "States_Running";
          case Stopped: return "States_Stopped";
          case Moving: return "States_Moving";

        }
        return "";
      }
    };
    int portM;
    int powM;
    int degM;
    lego::USB::Device* commM;
    bool relM;
    bool brakeM;
    bool timeoutBool;
    MotorDriver::States::type state;
    iMotor::MovingState::type reply_iMotor_MovingState;
    iMotor motor1;
    iTimer timer2;
    iActiveMotor client1;

    MotorDriver(const locator&);

    private:
    void client1_RotationCount(lego::USB::Device* comm, int port);
    void client1_ResetCount(lego::USB::Device* comm, int port, bool relative);
    void client1_Forward(lego::USB::Device* comm, int port, int power);
    void client1_Reverse(lego::USB::Device* comm, int port, int power);
    void client1_Stop(lego::USB::Device* comm, int port, bool brake);
    void client1_GoTo(lego::USB::Device* comm, int port, int power, int tacho, bool brake);
    void motor1_Count(int count);
    void timer2_timeout();
    bool greater(int rotationCount, int degM);
  };
}
#endif // DEZYNE_MOTORDRIVER_HH
