#include "Motor.hh"

#include "locator.hh"
#include "runtime.hh"

#include <cstdlib>

namespace dezyne
{
	Motor::Motor(const locator& loc)
		: rt(loc.get<runtime>())
		, port()
	{
		locator ts(loc.clone());
		ts.set(port);
		port.in.meta.component = "Motor";
		port.in.meta.port = "port";
		port.in.meta.address = this;

		port.in.SetForward = connect<lego::USB::Device*,int,int>(rt, this,
	    boost::function<void(lego::USB::Device*,int,int)>
	    ([&](lego::USB::Device* comm, int portM, int power){
			std::cout << "FORWARD on PORT: " << portM << std::endl;
			NXT::Motor::SetForward(comm, portM, power);
		}));

		port.in.SetReverse = connect<lego::USB::Device*,int, int>(rt, this,
	    boost::function<void(lego::USB::Device*,int, int)>
	    ([&](lego::USB::Device* comm, int portM, int power){
			std::cout << "REVERSE on PORT: " << portM << std::endl;
			NXT::Motor::SetReverse(comm, portM, power);
		}));

		port.in.GoTo = connect<lego::USB::Device*,int,int,int,bool>(rt, this,
	    boost::function<void(lego::USB::Device*,int,int,int,bool)>
	    ([&](lego::USB::Device* comm, int portM, int power, int deg, bool brake){

			int r = NXT::Motor::GetRotationCount(comm, portM);
			int delta = deg - r;
			auto sign = (delta < 0) ? -1 : 1;
			std::cout << "\033[34m_GOTO on PORT: " << portM << " pow: " << (sign * power) << " delta: "<< delta
			<< " deg: "<< deg << " r: " << r << "\033[0m" << std::endl;
			if( std::abs(delta) >= 1) NXT::Motor::GoTo(comm, portM, (sign * power), std::abs(delta), brake);
		}));

		port.in.ResetRotationCount = connect<lego::USB::Device*,int, bool>(rt, this,
	    boost::function<void(lego::USB::Device*,int, bool)>
	    ([&](lego::USB::Device* comm, int portM, bool rel){
			std::cout << "RESET on PORT: " << portM << std::endl;
			NXT::Motor::ResetRotationCount(comm, portM, rel);
			int r = NXT::Motor::GetRotationCount(comm, portM);
			std::cout << "After RESET-Rotation: "<< r << std::endl;
		}));

		port.in.Stop = connect<lego::USB::Device*,int, bool>(rt, this,
	    boost::function<void(lego::USB::Device*,int, bool)>
	    ([&](lego::USB::Device* comm, int portM, bool brake){
			std::cout << "STOP on PORT: " << portM << std::endl;
			NXT::Motor::Stop(comm, portM, brake);
		}));

		port.in.GetRotationCount = connect<lego::USB::Device*,int>(rt, this,
	        boost::function<void(lego::USB::Device*,int)>(
			[&](lego::USB::Device* comm, int portM){
			int r = NXT::Motor::GetRotationCount(comm, portM);
			// std::cout << "Rotation: "<< r  << " on PORT: " << portM << std::endl;
			// port.out.Count(r);
		}));


	}
}
