#ifndef DEZYNE_ACTIVESENSOR_HH
#define DEZYNE_ACTIVESENSOR_HH

#include "SensorDriver.hh"
#include "TouchSensor.hh"
#include "timer.hh"


#include "iActiveSensor.hh"


namespace dezyne
{
  struct locator;
}

namespace dezyne
{
  struct ActiveSensor
  {
    dezyne::meta meta;
    SensorDriver driver;
    TouchSensor sens;
    timer tim;

    iActiveSensor& port;

    ActiveSensor(const dezyne::locator&);
  };
}
#endif // DEZYNE_ACTIVESENSOR_HH
