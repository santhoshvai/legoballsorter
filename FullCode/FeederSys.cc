#include "FeederSys.hh"

namespace dezyne
{
  FeederSys::FeederSys(const dezyne::locator& dezyne_locator)
  : meta{{reinterpret_cast<component*>(&feederComp),reinterpret_cast<component*>(&ballsMotor),reinterpret_cast<component*>(&beltMotor),reinterpret_cast<component*>(&ballsSensor),reinterpret_cast<component*>(&beltSensor)}, 0, reinterpret_cast<component*>(this), ""}
  , feederComp(dezyne_locator)
  , ballsMotor(dezyne_locator)
  , beltMotor(dezyne_locator)
  , ballsSensor(dezyne_locator)
  , beltSensor(dezyne_locator)
  , port(feederComp.feeder)
  {
    feederComp.meta.parent = reinterpret_cast<component*>(this);
    feederComp.meta.address = reinterpret_cast<component*>(&feederComp);
    feederComp.meta.name = "feederComp";
    ballsMotor.meta.parent = reinterpret_cast<component*>(this);
    ballsMotor.meta.address = reinterpret_cast<component*>(&ballsMotor);
    ballsMotor.meta.name = "ballsMotor";
    beltMotor.meta.parent = reinterpret_cast<component*>(this);
    beltMotor.meta.address = reinterpret_cast<component*>(&beltMotor);
    beltMotor.meta.name = "beltMotor";
    ballsSensor.meta.parent = reinterpret_cast<component*>(this);
    ballsSensor.meta.address = reinterpret_cast<component*>(&ballsSensor);
    ballsSensor.meta.name = "ballsSensor";
    beltSensor.meta.parent = reinterpret_cast<component*>(this);
    beltSensor.meta.address = reinterpret_cast<component*>(&beltSensor);
    beltSensor.meta.name = "beltSensor";
    connect(ballsMotor.port, feederComp.ballsMotor);
    connect(beltMotor.port, feederComp.beltMotor);
    connect(ballsSensor.port, feederComp.ballsSensor);
    connect(beltSensor.port, feederComp.beltSensor);
  }
}
