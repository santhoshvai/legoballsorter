#ifndef DEZYNE_ISTAGE_HH
#define DEZYNE_ISTAGE_HH

#include <boost/bind.hpp>
#include <boost/function.hpp>
#include "lego_usb.hh"

namespace dezyne
{
  struct iStage
  {

    struct
    {
      boost::function<void (lego::USB::Device* commstage, lego::USB::Device* commfeed, lego::USB::Device* commconveyor)> init;
      boost::function<void ()> prepareStage;
      boost::function<void ()> unLoaded;

      struct
      {
        const char* component;
        const char* port;
        void*       address;
      } meta;
    } in;

    struct
    {
      boost::function<void ()> ballPass;
      boost::function<void ()> ballFail;

      struct
      {
        const char* component;
        const char* port;
        void*       address;
      } meta;
    } out;
  };

  inline void connect (iStage& provided, iStage& required)
  {
    assert (not required.in.init);
    assert (not required.in.prepareStage);
    assert (not required.in.unLoaded);

    assert (not provided.out.ballPass);
    assert (not provided.out.ballFail);

    provided.out = required.out;
    required.in = provided.in;
  }
}
#endif // DEZYNE_ISTAGE_HH
