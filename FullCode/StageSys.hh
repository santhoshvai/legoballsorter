#ifndef DEZYNE_STAGESYS_HH
#define DEZYNE_STAGESYS_HH

#include "Stage.hh"
#include "ActiveMotor.hh"
#include "ActiveMotor.hh"
#include "ActiveSensor.hh"
#include "ActiveSensor.hh"
#include "ActiveSensor.hh"
#include "ActiveSensor.hh"
#include "ColourSensor.hh"


#include "iStage.hh"


namespace dezyne
{
  struct locator;
}

namespace dezyne
{
  struct StageSys
  {
    dezyne::meta meta;
    Stage stageComp;
    ActiveMotor stageXMotor;
    ActiveMotor stageYMotor;
    ActiveSensor stageYHomeSensor;
    ActiveSensor stageYEndSensor;
    ActiveSensor stageXHomeSensor;
    ActiveSensor stageXEndSensor;
    ColourSensor lightSensor;

    iStage& port;

    StageSys(const dezyne::locator&);
  };
}
#endif // DEZYNE_STAGESYS_HH
