#include <iostream>
#include <string>
#include <map>
#include <csignal>

#include "Main.hh"
#include "locator.hh"
#include "runtime.hh"

#include "lego_usb.hh"

#include "pump.hh"
#include "itimer_impl.hh"

struct timer_impl: public itimer_impl
{
  static size_t g_id;
  size_t id;
  dezyne::iTimer& port;
  dezyne::pump& p;

  timer_impl(const dezyne::locator& l)
  : id(0)
  , port(l.get<dezyne::iTimer>())
  , p(l.get<dezyne::pump>())
  {}
  void create()
  {
    id = g_id++;
    // std::cout << "timer: " << id << " - CREATED " << std::endl;
    p.handle(id, 50, port.out.timeout);
  }
  void cancel()
  {
    std::cout << "timers.t" << id << "_cancel" << std::endl;
    p.remove(id);
  }
};
size_t timer_impl::g_id = 0;

lego::USB* lego_usb = 0;

void signal_handler(int sig)
{
  if(lego_usb)
  {
    for(auto& device: lego_usb->devices)
    {
      for(auto i = 0; i <= 2; ++i) device.coast(i);
      for(auto i = 0; i <= 3; ++i) device.set_input_mode(i, 0, 0);
    }
    std::cout << "Signal: " << sig << std::endl;
  }
  signal(sig, SIG_DFL);
  std::abort();
}

int main()
{
    signal(SIGINT, signal_handler);
    signal(SIGABRT, signal_handler);
    signal(SIGSEGV, signal_handler);
	dezyne::pump pump;
	dezyne::locator loc;
	dezyne::runtime rt;

	loc.set(rt);
	loc.set(pump);

	std::function<std::shared_ptr<itimer_impl>(const dezyne::locator&)> create_timer_impl = [](const dezyne::locator& l){return std::make_shared<timer_impl>(l);};
	loc.set(create_timer_impl);
    std::cout << " find all the bricks and their names "<< std::endl;
	// find all the bricks and their names
    lego::USB lego;
    lego_usb = &lego;
	std::map<std::string, lego::USB::Device*> bricks;
	for(auto& device: lego_usb->devices)
	{
		auto name = device.get_name();
		std::cout << "discovered: " << name << " at: " << &device << std::endl;
		bricks[name] = &device;
	}

	/* according to brick names store the pointers */
	lego::USB::Device* commPickUp = bricks.at("ROBOT");
	lego::USB::Device* commFeed = bricks.at("INPUT");
	lego::USB::Device* commStage = bricks.at("STAGE");
	lego::USB::Device* commConveyor = bricks.at("OUTPUT");

	struct initConveyor acceptStruct(commConveyor, IN_4, OUT_B, 50, false, true);
	struct initConveyor rejectStruct(commConveyor, IN_3, OUT_A, 50, false, true);

	//construct the component
	dezyne::Main main(loc);
    //dezyne::Feeder feed(loc);
	// implement the incomplete functions // all the outs if needed
    main.meta.parent = 0;
     main.meta.name = "main";

     main.port.out.meta.component = "main";
     main.port.out.meta.port = "main";
     main.port.out.meta.address = 0;

    //feed.feeder.out.ballFeeded = []{};
    //Eventloop
	pump.handle([&]{
        // register signal SIGSEGV and signal handler
		main.port.in.init(commPickUp, commFeed, commStage, commConveyor, &acceptStruct, &rejectStruct);
	});

    //std::cin.clear();
	//std::cin.ignore(255, '\n');
	std::cin.get();
	return 0;
}
