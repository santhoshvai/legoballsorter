#include "Conveyor.hh"

#include "locator.hh"
#include "runtime.hh"

#include <iostream>

namespace dezyne
{
  Conveyor::Conveyor(const locator& dezyne_locator)
  : rt(dezyne_locator.get<runtime>())
  , commConveyor()
  , InitConv()
  , portexitSensor()
  , portbeltMotor()
  , pow()
  , rel()
  , brake()
  , c(convState::Idle)
  , conveyor()
  , beltMotor()
  , exitSensor()
  {
    conveyor.in.meta.component = "Conveyor";
    conveyor.in.meta.port = "conveyor";
    conveyor.in.meta.address = this;
    beltMotor.out.meta.component = "Conveyor";
    beltMotor.out.meta.port = "beltMotor";
    beltMotor.out.meta.address = this;
    exitSensor.out.meta.component = "Conveyor";
    exitSensor.out.meta.port = "exitSensor";
    exitSensor.out.meta.address = this;

    conveyor.in.eject = connect<void>(rt, this,
    boost::function<void()>
    ([this] ()
    {
      trace (conveyor, "eject");
      conveyor_eject();
      trace_return (conveyor, "return");
      return;
    }
    ));
    conveyor.in.init = connect<struct initConveyor*>(rt, this,
    boost::function<void(struct initConveyor*)>
    ([this] (struct initConveyor* initConveyor)
    {
      trace (conveyor, "init");
      conveyor_init(initConveyor);
      trace_return (conveyor, "return");
      return;
    }
    ));
    beltMotor.out.taskPerformed=  [this] () {
      trace (beltMotor, "taskPerformed");
      rt.defer (beltMotor.in.meta.address, connect<void>(rt, this,
      boost::function<void()>(
      [=]
      {
        beltMotor_taskPerformed();
        return;
      }
      )));};
    exitSensor.out.sOpened=  [this] () {
      trace (exitSensor, "sOpened");
      rt.defer (exitSensor.in.meta.address, connect<void>(rt, this,
      boost::function<void()>(
      [=]
      {
        exitSensor_sOpened();
        return;
      }
      )));};
    exitSensor.out.sClosed=  [this] () {
      trace (exitSensor, "sClosed");
      rt.defer (exitSensor.in.meta.address, connect<void>(rt, this,
      boost::function<void()>(
      [=]
      {
        exitSensor_sClosed();
        return;
      }
      )));};
  }

  void Conveyor::conveyor_eject()
  {

    if (c == convState::Idle)
    {
      {
      }
    }
    else if (c == convState::Active)
    {
      {
        beltMotor.in.Forward(commConveyor, portbeltMotor, pow);
    //    beltMotor.in.GoTo(commConveyor, portbeltMotor, pow);
        // NXT::Motor::GoTo(commConveyor, portbeltMotor, pow, 1000, false);
        exitSensor.in.activate();
        std::cout << ("\033[33m_Conveyor: EJECT_ ||||||| FORWARD ON PORT: ") << portbeltMotor << "\033[0m" << std::endl;
      }
    }
  }

  void Conveyor::conveyor_init(struct initConveyor* initConveyor)
  {
    if (c == convState::Idle)
    {
      {
        struct initConveyor* convInit = initConveyor;
        {
          InitConv = convInit;
          init (InitConv);
          exitSensor.in.setTouch(commConveyor, portexitSensor);
          c = convState::Active;
        }
      }
    }
    else if (c == convState::Active)
    {
      {
      }
    }
  }

  void Conveyor::beltMotor_taskPerformed()
  {
    if (c == convState::Idle)
    {
      assert(false);
    }
    else if (c == convState::Active)
    {
      {
      }
    }
  }

  void Conveyor::exitSensor_sOpened()
  {
    if (c == convState::Idle)
    {
      assert(false);
    }
    else if (c == convState::Active)
    {
      {
      }
    }
  }

  void Conveyor::exitSensor_sClosed()
  {
      std::cout << ("\033[33m_Conveyor: exitSensor_sClosed_\033[0m") << std::endl;
    if (c == convState::Idle)
    {
      assert(false);
    }
    else if (c == convState::Active)
    {
      {

        std::cout << ("\033[33m_Conveyor: conveyor MOTOR STOP on PORT: ") << portbeltMotor << "\033[0m" << std::endl;
        beltMotor.in.Stop(commConveyor, portbeltMotor, false);
        exitSensor.in.stopMonitor();
      }
    }
    else {
        std::cout << ("\033[33m_Conveyor: exitSensor_sClosed_NOT WORKING\033[0m") << std::endl;
    }
  }

  void Conveyor::init(struct initConveyor* convInit)
  {
    commConveyor = convInit->_commPtr;
    portexitSensor = convInit->_portexitSensor;
    portbeltMotor = convInit->_portbeltMotor;
    pow = convInit->_pow;
    rel = convInit->_rel;
    brake = convInit->_brake;
    return;
  }

}
