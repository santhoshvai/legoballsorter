#ifndef DEZYNE_COLOURSENSOR_HH
#define DEZYNE_COLOURSENSOR_HH

#include "iColourSensor.hh"


#include "runtime.hh"

namespace dezyne
{
  struct locator;
  struct runtime;

  struct ColourSensor
  {
    dezyne::meta meta;
    runtime& rt;
    iColourSensor port;

    ColourSensor(const locator&);

    private:
    void port_GetValue(lego::USB::Device* commCS, int portCS);
    void port_SetTouch(lego::USB::Device* commCS, int portCS);
  };
}
#endif // DEZYNE_COLOURSENSOR_HH
