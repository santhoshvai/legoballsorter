#ifndef DEZYNE_IMOTOR_HH
#define DEZYNE_IMOTOR_HH

#include <boost/bind.hpp>
#include <boost/function.hpp>
#include "lego_usb.hh"

namespace dezyne
{
  struct iMotor
  {
    struct MovingState
    {
      enum type
      {
        Arrived, Moving
      };
      static const char* to_string(type v)
      {
        switch(v)
        {
          case Arrived: return "MovingState_Arrived";
          case Moving: return "MovingState_Moving";

        }
        return "";
      }
    };

    struct
    {
      boost::function<void (lego::USB::Device* commM, int port)> GetRotationCount;
      boost::function<void (lego::USB::Device* commM, int port, bool relative)> ResetRotationCount;
      boost::function<void (lego::USB::Device* commM, int port, int power)> SetForward;
      boost::function<void (lego::USB::Device* commM, int port, int power)> SetReverse;
      boost::function<void (lego::USB::Device* commM, int port, bool brake)> Stop;
      boost::function<void (lego::USB::Device* commM, int port, int power, int tacho, bool brake)> GoTo;

      struct
      {
        const char* component;
        const char* port;
        void*       address;
      } meta;
    } in;

    struct
    {
      boost::function<void (int count)> Count;

      struct
      {
        const char* component;
        const char* port;
        void*       address;
      } meta;
    } out;
  };

  inline void connect (iMotor& provided, iMotor& required)
  {
    assert (not required.in.GetRotationCount);
    assert (not required.in.ResetRotationCount);
    assert (not required.in.SetForward);
    assert (not required.in.SetReverse);
    assert (not required.in.Stop);
    assert (not required.in.GoTo);

    assert (not provided.out.Count);

    provided.out = required.out;
    required.in = provided.in;
  }
}
#endif // DEZYNE_IMOTOR_HH
