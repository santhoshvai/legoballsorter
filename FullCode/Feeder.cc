#include "Feeder.hh"

#include "locator.hh"
#include "runtime.hh"

#include <iostream>

namespace dezyne
{
  Feeder::Feeder(const locator& dezyne_locator)
  : rt(dezyne_locator.get<runtime>())
  , wafers(0)
  , commFeed()
  , commPickUp()
  , portballsSensor(IN_4)
  , porttrackLeftSensor(IN_3)
  , portbeltSensor(IN_2)
  , portPickUpSensor(IN_1)
  , portballM(OUT_C)
  , portBeltM(OUT_B)
  , pow(20)
  , degBallM(750)
  , degBeltM(150)
  , rel(false)
  , brake(true)
  , c(connState::Initialising)
  , feeder()
  , ballsMotor()
  , beltMotor()
  , ballsSensor()
  , beltSensor()
  {
    feeder.in.meta.component = "Feeder";
    feeder.in.meta.port = "feeder";
    feeder.in.meta.address = this;
    ballsMotor.out.meta.component = "Feeder";
    ballsMotor.out.meta.port = "ballsMotor";
    ballsMotor.out.meta.address = this;
    beltMotor.out.meta.component = "Feeder";
    beltMotor.out.meta.port = "beltMotor";
    beltMotor.out.meta.address = this;
    ballsSensor.out.meta.component = "Feeder";
    ballsSensor.out.meta.port = "ballsSensor";
    ballsSensor.out.meta.address = this;
    beltSensor.out.meta.component = "Feeder";
    beltSensor.out.meta.port = "beltSensor";
    beltSensor.out.meta.address = this;

    feeder.in.init = connect<lego::USB::Device*,lego::USB::Device*>(rt, this,
    boost::function<void(lego::USB::Device*,lego::USB::Device*)>
    ([this] (lego::USB::Device* commfeed, lego::USB::Device* commpickup)
    {
      trace (feeder, "init");
      feeder_init(commfeed,commpickup);
      trace_return (feeder, "return");
      return;
    }
    ));
    feeder.in.push = connect<void>(rt, this,
    boost::function<void()>
    ([this] ()
    {
      trace (feeder, "push");
      feeder_push();
      trace_return (feeder, "return");
      return;
    }
    ));
    ballsMotor.out.taskPerformed=  [this] () {
      trace (ballsMotor, "taskPerformed");
      rt.defer (ballsMotor.in.meta.address, connect<void>(rt, this,
      boost::function<void()>(
      [=]
      {
        ballsMotor_taskPerformed();
        return;
      }
      )));};
    beltMotor.out.taskPerformed=  [this] () {
      trace (beltMotor, "taskPerformed");
      rt.defer (beltMotor.in.meta.address, connect<void>(rt, this,
      boost::function<void()>(
      [=]
      {
        beltMotor_taskPerformed();
        return;
      }
      )));};
    ballsSensor.out.sOpened=  [this] () {
      trace (ballsSensor, "sOpened");
      rt.defer (ballsSensor.in.meta.address, connect<void>(rt, this,
      boost::function<void()>(
      [=]
      {
        ballsSensor_sOpened();
        return;
      }
      )));};
    ballsSensor.out.sClosed=  [this] () {
      trace (ballsSensor, "sClosed");
      rt.defer (ballsSensor.in.meta.address, connect<void>(rt, this,
      boost::function<void()>(
      [=]
      {
        ballsSensor_sClosed();
        return;
      }
      )));};
    beltSensor.out.sOpened=  [this] () {
      trace (beltSensor, "sOpened");
      rt.defer (beltSensor.in.meta.address, connect<void>(rt, this,
      boost::function<void()>(
      [=]
      {
        beltSensor_sOpened();
        return;
      }
      )));};
    beltSensor.out.sClosed=  [this] () {
      trace (beltSensor, "sClosed");
      rt.defer (beltSensor.in.meta.address, connect<void>(rt, this,
      boost::function<void()>(
      [=]
      {
        beltSensor_sClosed();
        return;
      }
      )));};
  }

  void Feeder::feeder_init(lego::USB::Device* commfeed, lego::USB::Device* commpickup)
  {
    if (c == connState::Initialising)
    {
      {
        commPickUp = commpickup;
        commFeed = commfeed;
        ballsSensor.in.setTouch(commFeed, portballsSensor);
        beltSensor.in.setTouch(commFeed, portbeltSensor);
        ballsSensor.in.activate();
        ballsMotor.in.Forward(commFeed, portballM, pow);
        c = connState::Calibrating;
      }
    }
    else if (not (c == connState::Initialising))
    {
      {
      }
    }
  }

  void Feeder::feeder_push()
  {
    if (c == connState::Initialising)
    {
      assert(false);
    }
    else if (c == connState::Active)
    {
      {
        wafers = wafers - 1;
        if (wafers == 0)
        {
          ballsSensor.in.activate();
          ballsMotor.in.Forward(commFeed, portballM, pow);
          c = connState::Calibrating;
        }
        else
        {

          ballsMotor.in.ResetCount(commFeed, portballM, rel);
          beltMotor.in.ResetCount(commFeed, portBeltM, rel);
          if (wafers > 1) ballsMotor.in.GoTo(commFeed, portballM, -pow, degBallM, brake);
          else ballsMotor.in.GoTo(commFeed, portballM, -pow, (degBallM-100), brake);
          beltSensor.in.activate();
        }
      }
    }
    else if (not ((c == connState::Active or c == connState::Initialising)))
    {
      {
      }
    }
  }

  void Feeder::ballsMotor_taskPerformed()
  {
    if (c == connState::Initialising)
    {
      assert(false);
    }
    else if (not (c == connState::Initialising))
    {
      {
      }
    }
  }

  void Feeder::beltMotor_taskPerformed()
  {
    if (c == connState::Initialising)
    {
      assert(false);
    }
    else if (c == connState::Active)
    {
      {
        feeder.out.ballFeeded();
      }
    }
    else if (not ((c == connState::Active or c == connState::Initialising)))
    {
      {
      }
    }
  }

  void Feeder::ballsSensor_sOpened()
  {
    if (c == connState::Initialising)
    {
      assert(false);
    }
    else if (not (c == connState::Initialising))
    {
      {
      }
    }
  }

  void Feeder::ballsSensor_sClosed()
  {
    if (c == connState::Initialising)
    {
      {
        assert(false);
      }
    }
    else if (c == connState::Calibrating)
    {
      {
        ballsMotor.in.Stop(commFeed, portballM, brake);
        ballsMotor.in.ResetCount(commFeed, portballM, rel);
        beltMotor.in.ResetCount(commFeed, portBeltM, rel);
        ballsSensor.in.stopMonitor();
        ballsMotor.in.GoTo(commFeed, portballM, -pow, degBallM, brake);
        wafers = 4;
        beltSensor.in.activate();
        c = connState::Active;
      }
    }
    else if (c == connState::Active)
    {
      {
      }
    }
  }

  void Feeder::beltSensor_sOpened()
  {
    if (c == connState::Initialising)
    {
      assert(false);
    }
    else if (not (c == connState::Initialising))
    {
      {
      }
    }
  }

  void Feeder::beltSensor_sClosed()
  {
    if (c == connState::Calibrating)
    {
      {
      }
    }
    else if (c == connState::Active)
    {
      {
         std::cout << ("\033[31m_BELTMOTOR GOTO_\033[0m") << std::endl;
        beltMotor.in.GoTo(commFeed, portBeltM, pow, degBeltM, brake);
        beltSensor.in.stopMonitor();
      }
    }
    else if (not ((c == connState::Calibrating or c == connState::Active)))
    assert(false);
  }


}
