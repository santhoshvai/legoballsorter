#include "TouchSensor.hh"

#include "locator.hh"
#include "runtime.hh"

namespace dezyne
{
	TouchSensor::TouchSensor(const locator& loc)
  : rt(loc.get<runtime>())
  , port()
  {
    locator ts(loc.clone());
    ts.set(port);
	port.in.meta.component = "TSensor";
	port.in.meta.port = "port";
	port.in.meta.address = this;

	port.in.SetTouch = connect<lego::USB::Device*,int>(rt, this,
    boost::function<void(lego::USB::Device*,int)>
    ([&](lego::USB::Device* commTS, int portTS){
		NXT::Sensor::SetTouch(commTS, portTS);
		std::cout << "Sensor " << portTS << " initialized" << std::endl;
	}));
	port.in.GetValue = connect<dezyne::iTouchSensor::SensorValue::type, lego::USB::Device*,int>(rt, this,
    boost::function<dezyne::iTouchSensor::SensorValue::type(lego::USB::Device*,int)>
    ([&](lego::USB::Device* commTS, int portTS){
		if (NXT::Sensor::GetValue(commTS, portTS) == 1) //if the touch sensor is pressed down
		{
			//std::cout << "Sensor pressed" << std::endl;
			return dezyne::iTouchSensor::SensorValue::Closed;
		}
		else
		{
			//std::cout << "Sensor not pressed" << std::endl;
			return dezyne::iTouchSensor::SensorValue::Opened;
		}
	}));
  }
}
