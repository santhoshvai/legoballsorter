#ifndef DEZYNE_TIMER_HH
#define DEZYNE_TIMER_HH

#include "iTimer.hh"


#include "runtime.hh"

namespace dezyne
{
  struct locator;
  struct runtime;

  struct timer
  {
    dezyne::meta meta;
    runtime& rt;
    iTimer port;

    timer(const locator&);

    private:
    void port_create();
    void port_cancel();
  };
}
#endif // DEZYNE_TIMER_HH
