#ifndef DEZYNE_ITIMER_HH
#define DEZYNE_ITIMER_HH

#include <boost/bind.hpp>
#include <boost/function.hpp>
#include "lego_usb.hh"

namespace dezyne
{
  struct iTimer
  {

    struct
    {
      boost::function<void ()> create;
      boost::function<void ()> cancel;

      struct
      {
        const char* component;
        const char* port;
        void*       address;
      } meta;
    } in;

    struct
    {
      boost::function<void ()> timeout;

      struct
      {
        const char* component;
        const char* port;
        void*       address;
      } meta;
    } out;
  };

  inline void connect (iTimer& provided, iTimer& required)
  {
    assert (not required.in.create);
    assert (not required.in.cancel);

    assert (not provided.out.timeout);

    provided.out = required.out;
    required.in = provided.in;
  }
}
#endif // DEZYNE_ITIMER_HH
