#ifndef DEZYNE_HANDLER_HH
#define DEZYNE_HANDLER_HH

#include "iMaterialHandler.hh"
#include "iActiveMotor.hh"
#include "iActiveSensor.hh"


#include "runtime.hh"

namespace dezyne
{
  struct locator;
  struct runtime;

  struct Handler
  {
    dezyne::meta meta;
    runtime& rt;
    struct connState
    {
      enum type
      {
        Initialising, Calibrating, Busy, PickUp, Drop, AfterPickUp
      };
      static const char* to_string(type v)
      {
        switch(v)
        {
          case Initialising: return "connState_Initialising";
          case Calibrating: return "connState_Calibrating";
          case Busy: return "connState_Busy";
          case PickUp: return "connState_PickUp";
          case Drop: return "connState_Drop";
          case AfterPickUp: return "connState_AfterPickUp";

        }
        return "";
      }
    };
    typedef int andInt;
    typedef int workInt;
    Handler::andInt ballHandlingCount;
    Handler::andInt prepareToUnloadStageInt;
    Handler::andInt goBackToInputPosInt;
    Handler::workInt workToDo;
    bool hoistCalibrated;
    bool trackCalibrated;
    bool trolleyCalibrated;
    bool ballBool;
    lego::USB::Device* commPickUp;
    lego::USB::Device* commFeed;
    lego::USB::Device* commConveyor;
    int portHoist;
    int portGripper;
    int portEndTrack;
    int portTrolleyM;
    int portPickUpSensor;
    int portExtenderHomeSwitchSensor;
    int porttrackLeftSensor;
    int portGripperSensor;
    int pow;
    int degEndTrackToGrabPosFromInput;
    int degEndTrackToStageBeforeInsp;
    int degEndTrackUnloadStage;
    int degEndTrackDeliverToConveyer;
    int degTrolleyGrabPosFromInput;
    int degTrolleyDeliverPosStageBeforeInsp;
    int degTrolleyUnloadStagePass;
    int degTrolleyUnloadStageFail;
    int degTrolleyDeliverToConveyerPass;
    int degTrolleyDeliverToConveyerFail;
    int degGripper;
    int degHoist;
    bool rel;
    bool brake;
    Handler::connState::type c;
    iMaterialHandler handler;
    iActiveMotor EndTrackMotor;
    iActiveMotor GripperMotor;
    iActiveMotor HoistMotor;
    iActiveMotor trolleyMotor;
    iActiveSensor trackLeftSensor;
    iActiveSensor pickupSensor;
    iActiveSensor ExtenderHomeSwitchSensor;
    iActiveSensor GripperSensor;

    Handler(const locator&);

    private:
    void handler_init(lego::USB::Device* commpickup, lego::USB::Device* commfeed, lego::USB::Device* commconveyor);
    void handler_ballFeeded();
    void handler_ballFail();
    void handler_ballPass();
    void EndTrackMotor_taskPerformed();
    void GripperMotor_taskPerformed();
    void HoistMotor_taskPerformed();
    void trolleyMotor_taskPerformed();
    void trackLeftSensor_sOpened();
    void trackLeftSensor_sClosed();
    void pickupSensor_sOpened();
    void pickupSensor_sClosed();
    void ExtenderHomeSwitchSensor_sOpened();
    void ExtenderHomeSwitchSensor_sClosed();
    void GripperSensor_sOpened();
    void GripperSensor_sClosed();
    void calibrate();
    void readycheck();
    void waitSync();
    void pickUpBall();
    void pickUpBallStep2();
    void pickUpBallStep2New();
    void dropBall();
    void dropBallStep2();
    void dropBallStep2New();
    void pickUpBallFromInput();
    void dropBallToStageBeforeInsp();
    void prepareToUnloadStage();
    void unloadBallFromStagePass();
    void unloadBallFromStageFail();
    void dropBallToConveyerPass();
    void dropBallToConveyerFail();
    void goBackToInputPos();
  };
}
#endif // DEZYNE_HANDLER_HH
