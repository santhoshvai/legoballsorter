#ifndef DEZYNE_HANDLERSYS_HH
#define DEZYNE_HANDLERSYS_HH

#include "Handler.hh"
#include "ActiveMotor.hh"
#include "ActiveMotor.hh"
#include "ActiveMotor.hh"
#include "ActiveMotor.hh"
#include "ActiveSensor.hh"
#include "ActiveSensor.hh"
#include "ActiveSensor.hh"
#include "ActiveSensor.hh"


#include "iMaterialHandler.hh"


namespace dezyne
{
  struct locator;
}

namespace dezyne
{
  struct HandlerSys
  {
    dezyne::meta meta;
    Handler handlerComp;
    ActiveMotor EndTrackMotor;
    ActiveMotor GripperMotor;
    ActiveMotor HoistMotor;
    ActiveMotor trolleyMotor;
    ActiveSensor trackLeftSensor;
    ActiveSensor pickupSensor;
    ActiveSensor ExtenderHomeSwitchSensor;
    ActiveSensor GripperSensor;

    iMaterialHandler& port;

    HandlerSys(const dezyne::locator&);
  };
}
#endif // DEZYNE_HANDLERSYS_HH
