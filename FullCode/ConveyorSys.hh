#ifndef DEZYNE_CONVEYORSYS_HH
#define DEZYNE_CONVEYORSYS_HH

#include "Conveyor.hh"
#include "ActiveMotor.hh"
#include "ActiveSensor.hh"


#include "iConveyor.hh"


namespace dezyne
{
  struct locator;
}

namespace dezyne
{
  struct ConveyorSys
  {
    dezyne::meta meta;
    Conveyor conveyor;
    ActiveMotor beltMotor;
    ActiveSensor exitSensor;

    iConveyor& port;

    ConveyorSys(const dezyne::locator&);
  };
}
#endif // DEZYNE_CONVEYORSYS_HH
