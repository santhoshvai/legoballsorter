#ifndef DEZYNE_TOUCHSENSOR_HH
#define DEZYNE_TOUCHSENSOR_HH

#include "iTouchSensor.hh"


#include "runtime.hh"

namespace dezyne
{
  struct locator;
  struct runtime;

  struct TouchSensor
  {
    dezyne::meta meta;
    runtime& rt;
    iTouchSensor::SensorValue::type reply_iTouchSensor_SensorValue;
    iTouchSensor port;

    TouchSensor(const locator&);

    private:
    iTouchSensor::SensorValue::type port_GetValue(lego::USB::Device* commTS, int portTS);
    void port_SetTouch(lego::USB::Device* commTS, int portTS);
  };
}
#endif // DEZYNE_TOUCHSENSOR_HH
