#ifndef DEZYNE_IMATERIALHANDLER_HH
#define DEZYNE_IMATERIALHANDLER_HH

#include <boost/bind.hpp>
#include <boost/function.hpp>
#include "lego_usb.hh"

namespace dezyne
{
  struct iMaterialHandler
  {

    struct
    {
      boost::function<void (lego::USB::Device* commpickup, lego::USB::Device* commfeed, lego::USB::Device* commconveyor)> init;
      boost::function<void ()> ballFeeded;
      boost::function<void ()> ballFail;
      boost::function<void ()> ballPass;

      struct
      {
        const char* component;
        const char* port;
        void*       address;
      } meta;
    } in;

    struct
    {
      boost::function<void ()> prepareStage;
      boost::function<void ()> unLoaded;
      boost::function<void ()> startConveyor;

      struct
      {
        const char* component;
        const char* port;
        void*       address;
      } meta;
    } out;
  };

  inline void connect (iMaterialHandler& provided, iMaterialHandler& required)
  {
    assert (not required.in.init);
    assert (not required.in.ballFeeded);
    assert (not required.in.ballFail);
    assert (not required.in.ballPass);

    assert (not provided.out.prepareStage);
    assert (not provided.out.unLoaded);
    assert (not provided.out.startConveyor);

    provided.out = required.out;
    required.in = provided.in;
  }
}
#endif // DEZYNE_IMATERIALHANDLER_HH
