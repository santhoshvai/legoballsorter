#include "ActiveSensor.hh"

namespace dezyne
{
  ActiveSensor::ActiveSensor(const dezyne::locator& dezyne_locator)
  : meta{{reinterpret_cast<component*>(&driver),reinterpret_cast<component*>(&sens),reinterpret_cast<component*>(&tim)}, 0, reinterpret_cast<component*>(this), ""}
  , driver(dezyne_locator)
  , sens(dezyne_locator)
  , tim(dezyne_locator)
  , port(driver.client1)
  {
    driver.meta.parent = reinterpret_cast<component*>(this);
    driver.meta.address = reinterpret_cast<component*>(&driver);
    driver.meta.name = "driver";
    sens.meta.parent = reinterpret_cast<component*>(this);
    sens.meta.address = reinterpret_cast<component*>(&sens);
    sens.meta.name = "sens";
    tim.meta.parent = reinterpret_cast<component*>(this);
    tim.meta.address = reinterpret_cast<component*>(&tim);
    tim.meta.name = "tim";
    connect(sens.port, driver.sensor1);
    connect(tim.port, driver.timer1);
  }
}
