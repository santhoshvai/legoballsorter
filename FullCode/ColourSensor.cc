#include "ColourSensor.hh"

#include "locator.hh"
#include "runtime.hh"

namespace dezyne
{
	ColourSensor::ColourSensor(const locator& loc)
		: rt(loc.get<runtime>())
		, port()
	{
		locator ts(loc.clone());
		ts.set(port);
		port.in.meta.component = "CSensor";
		port.in.meta.port = "port";
		port.in.meta.address = this;

		port.in.SetTouch = connect<lego::USB::Device*,int>(rt, this,
	    boost::function<void(lego::USB::Device*,int)>
	    ([&](lego::USB::Device* commCS, int portCS){
			NXT::Sensor::SetTouch(commCS, portCS);
			std::cout << "Colour Sensor initialized" << std::endl;
		}));
		port.in.GetValue = connect<lego::USB::Device*,int>(rt, this,
	    boost::function<void(lego::USB::Device*,int)>
	    ([&](lego::USB::Device* commCS, int portCS){
			NXT::ColourSensor::lightOn(commCS, portCS);
			sleep(1);
			if (NXT::ColourSensor::GetValue(commCS, portCS) == 1) //if the touch sensor is pressed down
			{
				//std::cout << "Sensor pressed" << std::endl;
				port.out.bluePass();
			}
			else
			{
				//std::cout << "Sensor not pressed" << std::endl;
				port.out.redFail();
			}
			NXT::ColourSensor::lightOff(commCS, portCS);
		}));

		// port.out.bluePass = []{
		// 	std::cout << "Pass" << std::endl;
		// };
		//
		// port.out.redFail = []{
		// 	std::cout << "Fail" << std::endl;
		// };

	}
}
