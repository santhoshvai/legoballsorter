#ifndef DEZYNE_MAIN_HH
#define DEZYNE_MAIN_HH

#include "Controller.hh"
#include "FeederSys.hh"
#include "StageSys.hh"
#include "HandlerSys.hh"
#include "ConveyorSys.hh"
#include "ConveyorSys.hh"


#include "IMain.hh"


namespace dezyne
{
  struct locator;
}

namespace dezyne
{
  struct Main
  {
    dezyne::meta meta;
    Controller MainCtrlr;
    FeederSys feeder;
    StageSys stage;
    HandlerSys handler;
    ConveyorSys accept;
    ConveyorSys reject;

    IMain& port;

    Main(const dezyne::locator&);
  };
}
#endif // DEZYNE_MAIN_HH
