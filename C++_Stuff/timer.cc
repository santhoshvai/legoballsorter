#include "timer.hh"

#include "locator.hh"
#include "runtime.hh"

#include "itimer_impl.hh"

#include <functional>
#include <memory>

namespace dezyne
{
  timer::timer(const locator& l)
  : rt(l.get<runtime>())
  , port()
  {
    locator tmp(l.clone());
    tmp.set(port);
    auto pimpl = l.get<std::function<std::shared_ptr<itimer_impl>(const locator&)>>()(tmp);
    port.in.create = [=]{pimpl->create();};
    port.in.cancel = [=]{pimpl->cancel();};
    port.out.timeout = []{};
  }
}
