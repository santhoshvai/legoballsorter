#include "ColourSensor.hh"

#include "locator.hh"
#include "runtime.hh"

namespace dezyne
{
	ColourSensor::ColourSensor(const locator& loc)
		: rt(loc.get<runtime>())
		, port()
	{
		locator ts(loc.clone());
		ts.set(port);
		port.in.SetTouch = [&](lego::USB::Device* commCS, int portCS){
			NXT::Sensor::SetTouch(commCS, portCS);
			std::cout << "Colour Sensor initialized" << std::endl;
		};
		port.in.GetValue = [&](lego::USB::Device* commCS, int portCS){
			if (NXT::ColourSensor::GetValue(commCS, portCS) == 1) //if the touch sensor is pressed down
			{
				//std::cout << "Sensor pressed" << std::endl;
				port.out.bluePass();
			}
			else
			{
				//std::cout << "Sensor not pressed" << std::endl;
				port.out.redFail();
			}
		};

		port.out.bluePass = []{
			std::cout << "Pass" << std::endl;
		};

		port.out.redFail = []{
			std::cout << "Fail" << std::endl;
		};

	}
}
