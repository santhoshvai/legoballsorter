#ifndef DEZYNE_TIMER_HH
#define DEZYNE_TIMER_HH

#include "iTimer.hh"


namespace dezyne
{
  struct locator;
  struct runtime;

  struct timer
  {
    runtime& rt;
    iTimer port;

    timer(const locator&);
    void port_create();
    void port_cancel();
  };
}
#endif // DEZYNE_TIMER_HH
