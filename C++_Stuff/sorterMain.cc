#include <iostream>
#include <string>
#include <map>

#include "Main.hh"
#include "locator.hh"
#include "runtime.hh"

#include "lego_usb.hh"

#include "pump.hh"
#include "itimer.hh"
#include "itimer_impl.hh"

struct timer_impl: public itimer_impl
{
  static size_t g_id;
  size_t id;
  dezyne::itimer& port;
  dezyne::pump& p;

  timer_impl(const dezyne::locator& l)
  : id(g_id++)
  , port(l.get<dezyne::itimer>())
  , p(l.get<dezyne::pump>())
  {}
  void create(int ms)
  {
    p.handle(id, ms, port.out.timeout);
  }
  void cancel()
  {
    std::cout << "timers.t" << id+1 << "_cancel" << std::endl;
    p.remove(id);
  }
};
size_t timer_impl::g_id = 0;

int main()
{

	dezyne::pump pump;
	dezyne::locator loc;
	dezyne::runtime rt;

	loc.set(rt);
	loc.set(pump);

	std::function<std::shared_ptr<itimer_impl>(const dezyne::locator&)> create_timer_impl = [](const dezyne::locator& loc){return std::make_shared<timer_impl>(loc); };
	loc.set(create_timer_impl);

	// find all the bricks and their names
	lego::USB lego_usb;
	std::map<std::string, lego::USB::Device*> bricks;
	for(auto& device: lego_usb.devices)
	{
		auto name = device.get_name();
		std::cout << "discovered: " << name << " at: " << &device << std::endl;
		bricks[name] = &device;
	}

	/* according to brick names store the pointers */
	lego::USB::Device* commPickUp = bricks["ROBOT"];
	lego::USB::Device* commFeed = bricks["INPUT"];
	lego::USB::Device* commStage = bricks["STAGE"];
	lego::USB::Device* commConveyor = bricks["OUTPUT"];

	dezyne::initConveyor acceptStruct(commConveyor, IN_4, OUT_B, 50, false, true);
	dezyne::initConveyor rejectStruct(commConveyor, IN_3, OUT_A, 50, false, true);

	//construct the component
	dezyne::Main main(loc);
	// implement the incomplete functions // all the outs if needed

	//Eventloop
	pump.handle([&]{
		main.Controller.in.Init(commPickUp, commFeed, commStage, commConveyor, &acceptStruct, &rejectStruct);
	});

    //std::cin.clear();
	//std::cin.ignore(255, '\n');
	std::cin.get();
	return 0;
}
