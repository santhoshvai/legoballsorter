#ifndef LOCATOR_H
#define LOCATOR_H

#include <map>
#include <stdexcept>
#include <string>
#include <typeinfo>

namespace dezyne {
class locator
{
  typedef std::string Key;
  struct type_info
  {
    const std::type_info* t;
    type_info(const std::type_info& t)
    : t(&t)
    {}
    bool operator < (const type_info& that) const
    {
      return t->before(*that.t);
    }
  };
  std::map<std::pair<Key,type_info>, void*> services;
public:
  locator clone() const
  {
    return locator(*this);
  }
  template <typename T>
  locator& set(T& t, const Key& key = Key())
  {
    services.insert(std::make_pair(std::make_pair(key,type_info(typeid(T))), &t));
    return *this;
  }
  template <typename T>
  T& get(const Key& key = Key()) const
  {
    std::map<std::pair<Key,type_info>, void*>::const_iterator it = services.find(std::make_pair(key,type_info(typeid(T))));
    if(it == services.end() || ! it->second) throw std::runtime_error("<" + std::string(typeid(T).name()) + ",\"" + key + "\"> not available");
    return *reinterpret_cast<T*>(it->second);
  }
};
}
#endif
