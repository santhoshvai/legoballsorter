#ifndef DEZYNE_ITIMER_HH
#define DEZYNE_ITIMER_HH

#include <boost/bind.hpp>
#include <boost/function.hpp>
#include "lego_usb.hh"
#include <cstdint>

namespace dezyne
{

  struct iTimer
  {

    struct
    {
      boost::function<void ()> create;
      boost::function<void ()> cancel;

    } in;

    struct
    {
      boost::function<void ()> timeout;

    } out;
  };
}
#endif // DEZYNE_ITIMER_HH
