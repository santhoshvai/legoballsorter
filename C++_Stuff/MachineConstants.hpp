#ifndef __MACHINE_CONSTANTS_HPP__
#define __MACHINE_CONSTANTS_HPP__

namespace Position
{
  namespace Robot
  {
    namespace Z // Hoist
    {
      constexpr int Length = 149; // not used
      constexpr int Up = -15;
      constexpr int Down = -110;
    }

    namespace Y // Trolley (Moves from Accept to Reject or from InputPort to Inspect)
    {
      constexpr int Length = 629; // not used
      constexpr int InputPick = -50;
      constexpr int OutputPick = -380;
      constexpr int InputDrop = -50;
      constexpr int RejectPick = OutputPick; // Between out ports
      constexpr int AcceptPick = OutputPick; // Between out ports
      constexpr int RejectDrop = -510;
      constexpr int AcceptDrop = -50;
    }

    namespace X // Truck (Moves from Input to Output)
    {
      constexpr int Length = 2668; // not used
      constexpr int InputPick = 90;
      constexpr int InputDrop = 810;
      constexpr int OutputPick = 1840;
      constexpr int OutputDrop = 2520;
    }
  }

  namespace Inspector
  {
    // Stage X home = at output port side
    // Stage Y home = robot track side
    constexpr int Margin = 30; // Margin to prevent home/end switch press
    constexpr int StageXLength = 1118;
    constexpr int StageYLength = 714;

    constexpr int ReceiveX = (StageXLength - Margin); // Brick STAGE, Port A
    constexpr int ReceiveY = Margin; // Brick STAGE, Port C
    constexpr int InspectX = (StageXLength - Margin);
    constexpr int InspectY = (StageYLength - Margin);
    constexpr int AcceptX = Margin;
    constexpr int AcceptY = (StageYLength) / 2;
    constexpr int RejectX = Margin;
    constexpr int RejectY = AcceptY;
  }

  namespace Feeder
  {
    //EndConstrained => negative range
    constexpr int Retracted = -150;
    constexpr int DropFirst = -450;
    constexpr int DropSecond = -1200;
    constexpr int DropThird = -1950;
    constexpr int DropLast = -2700;
  }
}

namespace Power
{
	constexpr int CalibrationSpeed = 20;
	constexpr int OperationalSpeed = 60;
  namespace Gripper
  {
    constexpr int Calibrate = 40; // 10;
    constexpr int Operational = 40;
  }
}

namespace Duration
{
  constexpr int PollInterval = 20; // milliseconds
  namespace Gripper
  {
    constexpr int CalibratePulse = 1300; // milliseconds
    constexpr int OperationalPulse = 1300; // milliseconds
  }
}

#endif
