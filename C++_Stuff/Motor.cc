#include "Motor.hh"

#include "locator.hh"
#include "runtime.hh"


namespace dezyne
{
	Motor::Motor(const locator& loc)
		: rt(loc.get<runtime>())
		, port()
	{
		locator ts(loc.clone());
		ts.set(port);

		port.in.SetForward = [&](lego::USB::Device* comm, int portM, int power){
			std::cout << "FORWARD" << std::endl;
			NXT::Motor::SetForward(comm, portM, power);
		};

		port.in.SetReverse = [&](lego::USB::Device* comm, int portM, int power){
			std::cout << "REVERSE" << std::endl;
			NXT::Motor::SetReverse(comm, portM, power);
		};

		port.in.GoTo = [&](lego::USB::Device* comm, int portM, int power, int deg, bool brake){
			std::cout << "GOTO" << std::endl;
			NXT::Motor::GoTo(comm, portM, power, deg, brake);
		};

		port.in.ResetRotationCount = [&](lego::USB::Device* comm, int portM, bool rel){
			std::cout << "RESET" << std::endl;
			NXT::Motor::ResetRotationCount(comm, portM, rel);
		};

		port.in.Stop = [&](lego::USB::Device* comm, int portM, bool brake){
			std::cout << "STOP" << std::endl;
			NXT::Motor::Stop(comm, portM, brake);
		};

		port.in.GetRotationCount = [&](lego::USB::Device* comm, int portM){
			int r = NXT::Motor::GetRotationCount(comm, portM);
			std::cout << "Rotation:"<< r << std::endl;
			port.out.Count(r);
		};

		port.out.Count = [&](int count){
			return count;
		};

	}
}
