#include "pump.hh"

#include <algorithm>
#include <iostream>

namespace dezyne
{
  pump::pump()
  : running(true)
  , task(std::async(std::launch::async, std::ref(*this)))
  {}
  pump::~pump()
  {
    std::unique_lock<std::mutex> lock(mutex);
    running = false;
    lock.unlock();
    condition.notify_one();
    task.wait();
  }
  void pump::operator()()
  {
    thread_id = std::this_thread::get_id();
    std::unique_lock<std::mutex> lock(mutex);
    while(running)
    {
      if(timers.empty())
      {
        condition.wait(lock, [this]{return queue.size() || ! running;});
      }
      else
      {
        condition.wait_until(lock, timers.begin()->first.t, [this]{return queue.size() || ! running;});
      }

      while(timers.size() && timers.begin()->first.expired())
      {
        auto t = *timers.begin();
        timers.erase(timers.begin());
        lock.unlock();
        t.second();
        lock.lock();
      }

      if(queue.size())
      {
        std::function<void()> f(queue.front());
        queue.pop();
        lock.unlock();
        f();
        lock.lock();
      }
    }
    assert(queue.empty());
  }
  void pump::handle(const std::function<void()>& e)
  {
    assert(e && std::this_thread::get_id() != thread_id);
    std::lock_guard<std::mutex> lock(mutex);
    queue.push(e);
    condition.notify_one();
  }
  void pump::handle(size_t id, size_t ms, const std::function<void()>& e)
  {
	  std::cout << "handle" << id << std::endl;

    assert(e && std::this_thread::get_id() == thread_id);
    assert(std::find_if(timers.begin(), timers.end(), [id](const std::pair<deadline, std::function<void()>>& p){ return p.first.id == id; }) == timers.end());
    auto p = timers.emplace(deadline(id, ms), e);
	assert(p.second);
  }
  void pump::remove(size_t id)
  {
    assert(std::this_thread::get_id() == thread_id);
    auto it = std::find_if(timers.begin(), timers.end(), [id](const std::pair<deadline, std::function<void()>>& p){ return p.first.id == id; });
    if(it != timers.end()) timers.erase(it);
  }
}
