#ifndef PUMP_HH
#define PUMP_HH

#include <cassert>
#include <condition_variable>
#include <functional>
#include <future>
#include <map>
#include <mutex>
#include <queue>

namespace dezyne
{
  struct pump
  {
    std::queue<std::function<void()>> queue;

    struct deadline
    {
      size_t id;
      std::chrono::steady_clock::time_point t;
      deadline(size_t id, size_t ms)
      : id(id)
      , t(std::chrono::steady_clock::now() + std::chrono::milliseconds(ms))
      {}
      bool expired() const
      {
        return t <= std::chrono::steady_clock::now();
      }
      bool operator < (const deadline& d) const
      {
		  return t < d.t || t == d.t && id < d.id;
      }
    };

    std::map<deadline, std::function<void()>> timers;

    std::thread::id thread_id;
    bool running;
    std::condition_variable condition;
    std::mutex mutex;
    std::future<void> task;
    pump();
    ~pump();
    void operator()();
    void handle(const std::function<void()>&);
    void handle(size_t id, size_t ms, const std::function<void()>&);
    void remove(size_t id);
  };
}

#endif
