#include "lego_usb.hh"

void NXT::Motor::SetForward(lego::USB::Device* comm, int portM, int power) {
    comm->move( portM, power, true, 0);
}

void NXT::Motor::SetReverse (lego::USB::Device* comm, int portM, int power) {
    comm->move( portM, -(power), true, 0);
}

void NXT::Motor::GoTo (lego::USB::Device* comm, int portM, int power, int deg, bool brake){
    brake = false;
    comm->move( portM, power, true, deg);
}

void NXT::Motor::ResetRotationCount (lego::USB::Device* comm, int portM, bool rel){
    rel = false;
    comm->zero( portM );
}

void NXT::Motor::Stop (lego::USB::Device* comm, int portM, bool brake){
    brake = false;
    comm->stop( portM );
}

int NXT::Motor::GetRotationCount (lego::USB::Device* comm, int portM){
    return comm->get_position(portM);
}

/* Sensor Functions */
void NXT::Sensor::SetTouch (lego::USB::Device* comm, int port) {
    return;
    // set_input_mode(std::uint8_t port, std::uint8_t type, std::uint8_t mode)
}

int NXT::Sensor::GetValue (lego::USB::Device* comm, int port){
    int value = comm->get_input_values(port);
    if ( value <= 200 ) return 1;
    else return 0;
}

/* Colour Sensor Functions */
int NXT::ColourSensor::GetValue (lego::USB::Device* comm, int port){
    int value = comm->get_input_values(port);
    if ( value <= 40 ) return 1;
    else return 0;
}
